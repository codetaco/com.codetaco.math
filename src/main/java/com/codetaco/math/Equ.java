package com.codetaco.math;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.support.DefaultEquationSupport;
import com.codetaco.math.impl.support.EquationSupport;
import com.codetaco.math.impl.token.TokVariable;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Create an instance of this class and send it an evaluate() message. The result will always be an instance of Double
 * or
 * String or ZonedDateTime.
 * <hr>
 * You can also register your own functions and operators by calling the static methods registerFunction and
 * registerOperator. These add to a static Map so there are then available for the duration of your application.
 * <hr>
 * A support instance can be handed to an instance of Equ with the setSupport() method. This must be done prior to
 * calling
 * evaluate. When a variable is found in the equation then the support instance is asked to instantiate the variable. It
 * is
 * expected that it will do what is necessary to come up with it. The test cases show a support instance that maintains
 * a
 * local Map of variable name / value pairs. But a database could be used as well.
 */
public class Equ<T> {

    private EquImpl equImpl;

    public static <B> Builder<B> builder(Class<B> returnType) {
        return new Builder<>();
    }

    public static class Builder<B> {
        private EquationSupport support;
        private String equation;
        private java.sql.Date baseDate;
        private Map<String, Object> variables = new HashMap<>();
        private Map<String, Class> functions = new HashMap<>();

        public Builder<B> support(EquationSupport support) {
            this.support = support;
            return this;
        }

        public Builder<B> equation(String equation) {
            this.equation = equation;
            return this;
        }

        public Builder<B> variable(String variableName, Object value) {
            variables.put(variableName, value);
            return this;
        }

        public Builder<B> function(String functionName, Class functionClass) {
            functions.put(functionName.toLowerCase(), functionClass);
            return this;
        }

        public Builder<B> baseDate(java.sql.Date baseDate) {
            this.baseDate = baseDate;
            return this;
        }

        public Equ<B> build() {
            EquImpl impl = new EquImpl();
            impl.initialize();
            functions.forEach((functionName, functionClass) -> {
                try {
                    impl.registerFunction(functionName, functionClass);
                } catch (Exception e) {
                    throw MathException.builder()
                            .cause(e)
                            .build();
                }
            });
            if (support == null) {
                impl.setSupport(new DefaultEquationSupport());
            } else {
                impl.setSupport(support);
            }
            variables.forEach((variableName, value) -> {
                try {
                    impl.getSupport().assignVariable(variableName, value);
                } catch (Exception e) {
                    throw MathException.builder()
                            .cause(e)
                            .build();
                }
            });
            impl.setBaseDate(baseDate);
            impl.compile(equation);
            return new Equ<>(impl);
        }

        public B evaluate() {
            return build().evaluate();
        }
    }

    private Equ(EquImpl equImpl) {
        this.equImpl = equImpl;
    }

    EquImpl getEquImpl() {
        return equImpl;
    }

    public T evaluate() {
        Object result = equImpl.evaluate();
        if (result instanceof TokVariable) {
            throw MathException.builder()
                    .cause(new Exception("unresolved variable: \""
                                           + ((TokVariable) result).getName()
                                           + "\""))
                    .build();
        }
        return (T) result;
    }

    public void variable(String variableName, Object value) {
        try {
            equImpl.getSupport().assignVariable(variableName, value);
        } catch (Exception e) {
            throw MathException.builder().cause(e).build();
        }
    }

    public String getString(String variableName) {
        try {
            Object value = equImpl.getSupport().resolveVariable(variableName);
            if (value instanceof Double) {
                return value.toString();
            }
            if (value instanceof Integer) {
                return ((Integer) value).toString();
            }
            if (value instanceof Long) {
                return ((Long) value).toString();
            }
            return (String) value;
        } catch (Exception e) {
            throw MathException.builder()
                    .cause(e)
                    .build();
        }
    }

    @Override
    public String toString() {
        return equImpl.toString();
    }

    public Long getLong(String variableName) {
        try {
            Object value = equImpl.getSupport().resolveVariable(variableName);
            if (value instanceof Double) {
                return ((Double) value).longValue();
            }
            if (value instanceof Integer) {
                return ((Integer) value).longValue();
            }
            return (Long) value;
        } catch (Exception e) {
            throw MathException.builder()
                    .cause(e)
                    .build();
        }
    }

    public Integer getInteger(String variableName) {
        try {
            Object value = equImpl.getSupport().resolveVariable(variableName);
            if (value instanceof Double) {
                return ((Double) value).intValue();
            }
            if (value instanceof Long) {
                return ((Long) value).intValue();
            }
            return (Integer) value;
        } catch (Exception e) {
            throw MathException.builder()
                    .cause(e)
                    .build();
        }
    }

    public ZonedDateTime getZonedDateTime(String variableName) {
        try {
            Object value = equImpl.getSupport().resolveVariable(variableName);
            return (ZonedDateTime) value;
        } catch (Exception e) {
            throw MathException.builder()
                    .cause(e)
                    .build();
        }
    }

    public Double getDouble(String variableName) {
        try {
            Object value = equImpl.getSupport().resolveVariable(variableName);
            if (value instanceof Integer) {
                return ((Integer) value).doubleValue();
            }
            if (value instanceof Long) {
                return ((Long) value).doubleValue();
            }
            return (Double) value;
        } catch (Exception e) {
            throw MathException.builder()
                    .cause(e)
                    .build();
        }
    }

    public String showRPN() {
        return equImpl.showRPN();
    }
}
