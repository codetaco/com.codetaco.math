package com.codetaco.math;

public class MathException extends RuntimeException {

    public static class Builder {

        private Throwable cause;

        public Builder cause(Throwable cause) {
            this.cause = cause;
            return this;
        }

        public MathException build() {
            if (cause instanceof MathException) {
                return (MathException) cause;
            }
            return new MathException(cause);
        }
    }

    static public Builder builder() {
        return new Builder();
    }

    private MathException() {
    }

    private MathException(String message) {
        super(message);
    }

    private MathException(String message, Throwable cause) {
        super(message, cause);
    }

    private MathException(Throwable cause) {
        super(cause);
    }

    @Override
    public String getMessage() {
        return getCause().getMessage();
    }

    private MathException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
