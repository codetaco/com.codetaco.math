package com.codetaco.math.impl.support;

import com.codetaco.date.CalendarFactory;
import com.codetaco.math.impl.token.TokVariableWithValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

public class DefaultEquationSupport implements EquationSupport {

    static class Variable {
        boolean systemGenerated;
        Object value;
        String name;

        public Variable(String _name, Object _value) {
            this(_name, _value, false);
        }

        public Variable(String _name, Object _value, boolean sysgen) {
            name = _name;
            value = _value;
            systemGenerated = sysgen;
        }
    }

    private final static Logger logger = LoggerFactory.getLogger(DefaultEquationSupport.class.getName());
    private Hashtable<String, Variable> variables;

    public DefaultEquationSupport() {
        super();
        setVariables(new Hashtable<>());
        initializeWellKnownVariables();
    }

    @Override
    public void assignVariable(String variableName, Object value) throws Exception {
        assignVariable(variableName, value, false);
    }

    @Override
    public void assignVariable(String variableName, Object value, boolean sysgen) throws Exception {
        /*
         * Only store supported types in the stack.
         */
        if (value instanceof Date) {
            getVariables().put(variableName.toLowerCase(),
                               new Variable(variableName, CalendarFactory.asZoned((Date) value), sysgen));
        } else if (value instanceof Calendar) {
            getVariables().put(variableName.toLowerCase(),
                               new Variable(variableName, CalendarFactory.asZoned((Calendar) value), sysgen));
        } else if (value instanceof LocalDateTime) {
            getVariables().put(variableName.toLowerCase(),
                               new Variable(variableName, CalendarFactory.asZoned((LocalDateTime) value), sysgen));
        } else if (value instanceof LocalDate) {
            getVariables().put(variableName.toLowerCase(),
                               new Variable(variableName, CalendarFactory.asZoned((LocalDate) value), sysgen));
        } else if (value instanceof LocalTime) {
            getVariables().put(variableName.toLowerCase(),
                               new Variable(variableName, CalendarFactory.asZoned((LocalTime) value), sysgen));
        } else {
            getVariables().put(variableName.toLowerCase(),
                               new Variable(variableName, value, sysgen));
        }
    }

    @Override
    public void clear() {
        Enumeration<Variable> varIter = getVariables().elements();
        while (varIter.hasMoreElements()) {
            Variable var = varIter.nextElement();
            if (var.systemGenerated) {
                continue;
            }
            removeVariable(var.name);
        }
    }

    @Override
    public Set<String> getVariableNames() {
        return getVariableNames(true);
    }

    @Override
    public Set<String> getVariableNames(boolean includeSystemGenerated) {

        if (includeSystemGenerated) {
            return variables.keySet();
        }
        Set<String> names = new HashSet<>();
        for (String name : variables.keySet()) {
            if (!getVariables().get(name).systemGenerated) {
                names.add(name);
            }
        }
        return names;
    }

    private Hashtable<String, Variable> getVariables() {
        return variables;
    }

    private void initializeWellKnownVariables() {
        try {
            assignVariable("PI", Math.PI, true);
            assignVariable("E", Math.E, true);
            assignVariable("true", new Boolean(true), true);
            assignVariable("false", new Boolean(false), true);
            assignVariable("now", "now", true);
            assignVariable("today", "today", true);

        } catch (Exception e) {
            logger.error("failed to initialize some well known variables: {}", e.getMessage(), e);
        }
    }

    @Override
    public void removeVariable(String name) {
        getVariables().remove(name.toLowerCase());
    }

    @Override
    public Hashtable<Double, Double> resolveRate(String tableName,
                                                 java.util.Date baseDate,
                                                 double tableKey) {
        Hashtable<Double, Double> rates = new Hashtable<>();
        rates.put(new Double(0), new Double(1));

        return rates;
    }

    @Override
    public double resolveRate(String tableId,
                              java.util.Date effectiveDate,
                              String key1,
                              String key2,
                              String key3,
                              String key4,
                              String key5) {
        return 1D;
    }

    @Override
    public Object resolveVariable(String variableName) {
        return resolveVariable(variableName, null);
    }

    @Override
    public Object resolveVariable(String variableName,
                                  java.util.Date baseDate) {
        Variable variable = getVariables().get(variableName.toLowerCase());
        if (variable == null) {
            return null;
        }
        if (variable.value instanceof TokVariableWithValue) {
            return ((TokVariableWithValue) variable.value).getCurrentValue();
        }
        return variable.value;
    }

    public void setVariables(Hashtable<String, Variable> newVariables) {
        variables = newVariables;
    }
}
