package com.codetaco.math.impl;

import com.codetaco.math.impl.operator.OpComma;
import com.codetaco.math.impl.token.TokVariable;

public abstract class Function extends Operation {
    private int parameterCount;

    public Function(EquImpl equ) {
        super(equ);
    }

    public Function(EquImpl equ, TokVariable var) {
        super(equ);
        setLevel(var.getLevel());
    }

    public int getParameterCount() {
        return parameterCount;
    }

    @Override
    protected int precedence() {
        return 2;
    }

    public void setParameterCount(final int newParameterCount) {
        parameterCount = newParameterCount;
    }

    public void updateParameterCount(final EquPart[] equParts, final int myLocInArray) {
        setParameterCount(0);

        for (int p = myLocInArray + 1; p < equParts.length; p++) {
            final EquPart part = equParts[p];

            if (part.getLevel() <= getLevel()) {
                break;
            }

            if ((part.getLevel() == (getLevel() + 1)) && part instanceof OpComma) {
                setParameterCount(getParameterCount() + 1);
            }
        }

        if (getParameterCount() > 0) {
            setParameterCount(getParameterCount() + 1);
        }
    }
}
