package com.codetaco.math.impl;

public abstract class EquPart extends Object {

    private EquImpl equ;
    private int level;

    public EquPart(EquImpl equImpl) {
        super();
        equ = equImpl;
    }

    public EquImpl getEqu() {
        return equ;
    }

    public int getLevel() {
        return level;
    }

    public boolean multiplize(final EquPart rightSide) {
        return false;
    }

    public boolean negatize(final EquPart rightSide) {
        return false;
    }

    public abstract void resolve(ValueStack values) throws Exception;

    public void setLevel(final int newLevel) {
        level = newLevel;
    }
}
