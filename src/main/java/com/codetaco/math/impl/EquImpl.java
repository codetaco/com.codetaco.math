package com.codetaco.math.impl;

import com.codetaco.math.MathException;
import com.codetaco.math.impl.function.FuncAbs;
import com.codetaco.math.impl.function.FuncAcos;
import com.codetaco.math.impl.function.FuncAcotan;
import com.codetaco.math.impl.function.FuncAlpha;
import com.codetaco.math.impl.function.FuncAsin;
import com.codetaco.math.impl.function.FuncAtan;
import com.codetaco.math.impl.function.FuncBandedRate;
import com.codetaco.math.impl.function.FuncBytesToHex;
import com.codetaco.math.impl.function.FuncCos;
import com.codetaco.math.impl.function.FuncCubeRoot;
import com.codetaco.math.impl.function.FuncDate;
import com.codetaco.math.impl.function.FuncDateTime;
import com.codetaco.math.impl.function.FuncDegreesToRads;
import com.codetaco.math.impl.function.FuncFlatRate;
import com.codetaco.math.impl.function.FuncHaversine;
import com.codetaco.math.impl.function.FuncIf;
import com.codetaco.math.impl.function.FuncKm2Mi;
import com.codetaco.math.impl.function.FuncLog;
import com.codetaco.math.impl.function.FuncLog10;
import com.codetaco.math.impl.function.FuncMax;
import com.codetaco.math.impl.function.FuncMi2Km;
import com.codetaco.math.impl.function.FuncMin;
import com.codetaco.math.impl.function.FuncNot;
import com.codetaco.math.impl.function.FuncRadsToDegrees;
import com.codetaco.math.impl.function.FuncRoot;
import com.codetaco.math.impl.function.FuncRound;
import com.codetaco.math.impl.function.FuncSin;
import com.codetaco.math.impl.function.FuncSqrt;
import com.codetaco.math.impl.function.FuncStringCat;
import com.codetaco.math.impl.function.FuncStringContains;
import com.codetaco.math.impl.function.FuncStringEmpty;
import com.codetaco.math.impl.function.FuncStringIndexOf;
import com.codetaco.math.impl.function.FuncStringLTrim;
import com.codetaco.math.impl.function.FuncStringLength;
import com.codetaco.math.impl.function.FuncStringLowerCase;
import com.codetaco.math.impl.function.FuncStringMatch;
import com.codetaco.math.impl.function.FuncStringMatches;
import com.codetaco.math.impl.function.FuncStringMetaphone;
import com.codetaco.math.impl.function.FuncStringRTrim;
import com.codetaco.math.impl.function.FuncStringReplace;
import com.codetaco.math.impl.function.FuncStringSubstr;
import com.codetaco.math.impl.function.FuncStringTrim;
import com.codetaco.math.impl.function.FuncStringUpCase;
import com.codetaco.math.impl.function.FuncTan;
import com.codetaco.math.impl.function.FuncTieredRate;
import com.codetaco.math.impl.function.FuncTime;
import com.codetaco.math.impl.function.FuncToDate;
import com.codetaco.math.impl.function.FuncToDateTime;
import com.codetaco.math.impl.function.FuncToDouble;
import com.codetaco.math.impl.function.FuncToFloat;
import com.codetaco.math.impl.function.FuncToInt;
import com.codetaco.math.impl.function.FuncToLong;
import com.codetaco.math.impl.function.FuncToString;
import com.codetaco.math.impl.function.FuncToTime;
import com.codetaco.math.impl.function.FuncTrunc;
import com.codetaco.math.impl.operator.OpAdd;
import com.codetaco.math.impl.operator.OpAnd;
import com.codetaco.math.impl.operator.OpAssignment;
import com.codetaco.math.impl.operator.OpAssignmentAdd;
import com.codetaco.math.impl.operator.OpAssignmentDivide;
import com.codetaco.math.impl.operator.OpAssignmentMinus;
import com.codetaco.math.impl.operator.OpAssignmentMultiply;
import com.codetaco.math.impl.operator.OpChain;
import com.codetaco.math.impl.operator.OpComma;
import com.codetaco.math.impl.operator.OpCompareEqual;
import com.codetaco.math.impl.operator.OpCompareGreater;
import com.codetaco.math.impl.operator.OpCompareLess;
import com.codetaco.math.impl.operator.OpCompareNotEqual;
import com.codetaco.math.impl.operator.OpCompareNotGreater;
import com.codetaco.math.impl.operator.OpCompareNotLess;
import com.codetaco.math.impl.operator.OpDivide;
import com.codetaco.math.impl.operator.OpFactorial;
import com.codetaco.math.impl.operator.OpLeftParen;
import com.codetaco.math.impl.operator.OpMinus;
import com.codetaco.math.impl.operator.OpMinusMinus;
import com.codetaco.math.impl.operator.OpMod;
import com.codetaco.math.impl.operator.OpMultiply;
import com.codetaco.math.impl.operator.OpNand;
import com.codetaco.math.impl.operator.OpNegate;
import com.codetaco.math.impl.operator.OpNor;
import com.codetaco.math.impl.operator.OpOr;
import com.codetaco.math.impl.operator.OpPlusPlus;
import com.codetaco.math.impl.operator.OpPower;
import com.codetaco.math.impl.operator.OpRightParen;
import com.codetaco.math.impl.operator.OpXnor;
import com.codetaco.math.impl.operator.OpXor;
import com.codetaco.math.impl.support.DefaultEquationSupport;
import com.codetaco.math.impl.support.EquationSupport;
import com.codetaco.math.impl.token.TokLiteral;
import com.codetaco.math.impl.token.TokVariable;
import com.codetaco.math.impl.token.TokVariableWithValue;
import com.codetaco.math.impl.token.Token;
import org.apache.commons.codec.language.Metaphone;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.stream.IntStream;

/**
 * Create an instance of this class and send it an evaluate() message. The result will always be an instance of Double
 * or
 * String or ZonedDateTime.
 * <hr>
 * You can also register your own functions and operators by calling the static methods registerFunction and
 * registerOperator. These add to a static Map so there are then available for the duration of your application.
 * <hr>
 * A support instance can be handed to an instance of Equ with the setSupport() method. This must be done prior to
 * calling
 * evaluate. When a variable is found in the equation then the support instance is asked to instantiate the variable. It
 * is
 * expected that it will do what is necessary to come up with it. The test cases show a support instance that maintains
 * a
 * local Map of variable name / value pairs. But a database could be used as well.
 */
public class EquImpl {
    private static EquImpl instance;

    public Set<String> gatherVariables() {
        Set<String> vars = new HashSet<>();
        for (EquPart token : tokens) {
            if (token instanceof TokVariable) {
                vars.add(((TokVariable) token).getValue().toString());
            }
        }
        return vars;
    }

    public static EquImpl getInstance(EquationSupport support) {
        EquImpl equ = getInstance(true);
        return equ.setSupport(support);
    }

    public static EquImpl getInstance() {
        return getInstance(false);
    }

    public static EquImpl getInstance(boolean fresh) {
        if (instance == null || fresh) {
            EquImpl newInstance = new EquImpl();
            try {
                newInstance.initialize();
            } catch (Exception e) {
                e.printStackTrace();
            }
            instance = newInstance;
        }
        return instance;
    }

    private Map<String, Constructor<?>> functionMap;
    private Map<String, Constructor<?>> operatorMap;
    private java.sql.Date baseDate;
    private EquationSupport support;
    private String equ;
    private List<EquPart> rpn;
    private List<EquPart> tokens;
    private Set<String> variablesThatExistedBeforePreviousEvaluation;
    private Metaphone cachedMetaphone;

    public EquImpl() {
        super();
    }

    public void compile(String _equ) {
        equ = _equ;
        tokens = tokenize();
        tokens = minusMinus(tokens);
        tokens = negatize(tokens);
        tokens = multiplize(tokens);
        countParameters(tokens);
        rpn = rpnize(tokens);
    }

    void countParameters(List<EquPart> oldTokens) {
        EquPart[] equParts = oldTokens.toArray(new EquPart[0]);
        /*
         * ... func(p1, p2 ... ,pN) ... find out what N is
         */
        for (int f = 0; f < equParts.length; f++) {
            if (equParts[f] instanceof Function) {
                ((Function) equParts[f]).updateParameterCount(equParts, f);
            }
        }
    }

    public Object evaluate() {
        try {
            ValueStack values = new ValueStack();

            if (rpn == null) {
                return null;
            }

            if (variablesThatExistedBeforePreviousEvaluation != null) {
                for (String varName : getSupport().getVariableNames()) {
                    if (variablesThatExistedBeforePreviousEvaluation.contains(varName)) {
                        continue;
                    }
                    getSupport().removeVariable(varName);
                }
            }
            /*
             * Save variable names that exist before the equation. If the equation
             * assigns values to variables then they will be new names.
             */
            variablesThatExistedBeforePreviousEvaluation = getSupport().getVariableNames();

            for (Iterator<EquPart> parts = rpn.iterator(); parts.hasNext(); ) {
                EquPart part = parts.next();
                part.resolve(values);
            }

            if (values.isEmpty()) {
                return null;
            }

            Object result = values.firstElement();
            values.clear();

            if (result instanceof TokVariableWithValue) {
                return ((TokVariableWithValue) result).getCurrentValue();
            }

            return result;
        } catch (Exception e) {
            throw MathException.builder().cause(e).build();
        }
    }

    public Object evaluate(String _equ) {
        compile(_equ);
        return evaluate();
    }

    public Function function(TokVariable varTok) {
        String token = varTok.getValue().toString();
        Constructor<?> constructor = functionMap.get(token.toLowerCase());
        if (constructor == null) {
            return null;
        }
        try {
            return (Function) constructor.newInstance(this, varTok);
        } catch (Exception e) {
            throw MathException.builder().cause(new Exception("function construction", e)).build();
        }
    }

    public java.sql.Date getBaseDate() {
        return baseDate;
    }

    public Metaphone getMetaphone() {
        if (cachedMetaphone == null) {
            cachedMetaphone = new Metaphone();
        }
        return cachedMetaphone;
    }

    public EquationSupport getSupport() {
        if (support == null) {
            setSupport(new DefaultEquationSupport());
            initializeSupport();
        }
        return support;
    }

    public void initialize() {
        functionMap = new Hashtable<>();
        registerFunction("if", FuncIf.class);
        registerFunction("rate", FuncFlatRate.class);
        registerFunction("bandedrate", FuncBandedRate.class);
        registerFunction("tieredrate", FuncTieredRate.class);
        registerFunction("round", FuncRound.class);
        registerFunction("alpha", FuncAlpha.class);
        registerFunction("max", FuncMax.class);
        registerFunction("min", FuncMin.class);
        registerFunction("sin", FuncSin.class);
        registerFunction("tan", FuncTan.class);
        registerFunction("abs", FuncAbs.class);
        registerFunction("acos", FuncAcos.class);
        registerFunction("acotan", FuncAcotan.class);
        registerFunction("asin", FuncAsin.class);
        registerFunction("atan", FuncAtan.class);
        registerFunction("cos", FuncCos.class);
        registerFunction("deg", FuncRadsToDegrees.class);
        registerFunction("rad", FuncDegreesToRads.class);
        registerFunction("root", FuncRoot.class);
        registerFunction("sqrt", FuncSqrt.class);
        registerFunction("cbrt", FuncCubeRoot.class);
        registerFunction("log", FuncLog.class);
        registerFunction("log10", FuncLog10.class);
        registerFunction("trunc", FuncTrunc.class);
        registerFunction("not", FuncNot.class);
        registerFunction("match", FuncStringMatch.class);
        registerFunction("matches", FuncStringMatches.class);
        registerFunction("contains", FuncStringContains.class);
        registerFunction("empty", FuncStringEmpty.class);
        registerFunction("isempty", FuncStringEmpty.class);
        registerFunction("cat", FuncStringCat.class);
        registerFunction("length", FuncStringLength.class);
        registerFunction("substr", FuncStringSubstr.class);
        registerFunction("rtrim", FuncStringRTrim.class);
        registerFunction("ltrim", FuncStringLTrim.class);
        registerFunction("trim", FuncStringTrim.class);
        registerFunction("indexOf", FuncStringIndexOf.class);
        registerFunction("date", FuncDate.class);
        registerFunction("time", FuncTime.class);
        registerFunction("dateTime", FuncDateTime.class);
        registerFunction("toDateTime", FuncToDateTime.class);
        registerFunction("toDate", FuncToDate.class);
        registerFunction("toTime", FuncToTime.class);
        registerFunction("toString", FuncToString.class);
        registerFunction("ucase", FuncStringUpCase.class);
        registerFunction("lcase", FuncStringLowerCase.class);
        registerFunction("metaphone", FuncStringMetaphone.class);
        registerFunction("toInt", FuncToInt.class);
        registerFunction("toFloat", FuncToFloat.class);
        registerFunction("toLong", FuncToLong.class);
        registerFunction("toDouble", FuncToDouble.class);
        registerFunction("toHex", FuncBytesToHex.class);
        registerFunction("replace", FuncStringReplace.class);
        registerFunction("haversine", FuncHaversine.class);
        registerFunction("mi2km", FuncMi2Km.class);
        registerFunction("km2mi", FuncKm2Mi.class);

        operatorMap = new Hashtable<>();
        registerOperator("^", OpPower.class);
        registerOperator(",", OpComma.class);
        registerOperator(";", OpChain.class);
        registerOperator("*", OpMultiply.class);
        registerOperator("/", OpDivide.class);
        registerOperator("+", OpAdd.class);
        registerOperator("-", OpMinus.class);
        registerOperator("(", OpLeftParen.class);
        registerOperator(")", OpRightParen.class);
        registerOperator("=", OpCompareEqual.class);
        registerOperator("!=", OpCompareNotEqual.class);
        registerOperator(">", OpCompareGreater.class);
        registerOperator("<=", OpCompareNotGreater.class);
        registerOperator("<", OpCompareLess.class);
        registerOperator(">=", OpCompareNotLess.class);
        registerOperator(":=", OpAssignment.class);
        registerOperator("&&", OpAnd.class);
        registerOperator("!&", OpNand.class);
        registerOperator("||", OpOr.class);
        registerOperator("!|", OpNor.class);
        registerOperator("~|", OpXor.class);
        registerOperator("!~|", OpXnor.class);
        registerOperator("%", OpMod.class);
        registerOperator("!", OpFactorial.class);
        registerOperator("+=", OpAssignmentAdd.class);
        registerOperator("-=", OpAssignmentMinus.class);
        registerOperator("*=", OpAssignmentMultiply.class);
        registerOperator("/=", OpAssignmentDivide.class);
        registerOperator("++", OpPlusPlus.class);
        registerOperator("--", OpMinusMinus.class);
    }

    private void initializeSupport() {
        try {
            getSupport();

        } catch (Exception e) {
            /*
             * There is no reason to expect an error here even though one can
             * possibly be thrown.
             */
            e.printStackTrace();
        }
    }

    private List<EquPart> multiplize(List<EquPart> oldTokens) {
        EquPart[] equParts = oldTokens.toArray(new EquPart[0]);
        EquPart[] fixed = new EquPart[equParts.length * 2];
        /*
         * )(, operand (, operand operand, operand function, ) operand, )
         * function, digit E end
         */
        fixed[0] = equParts[0];

        EquPart m;
        int left = 0;

        for (int right = 1; right < equParts.length; right++) {
            if (fixed[left].multiplize(equParts[right])) {
                m = new OpMultiply(this, fixed[left]);
                left++;
                fixed[left] = m;
            }

            left++;
            fixed[left] = equParts[right];
        }

        List<EquPart> tokens = new ArrayList<>();

        IntStream.range(0, fixed.length)
          .filter(i -> fixed[i] != null)
          .forEach(i -> tokens.add(fixed[i]));

        return tokens;
    }

    private List<EquPart> minusMinus(List<EquPart> equParts) {
        List<EquPart> newList = new ArrayList<>();
        for (int right = 1; right <= equParts.size(); ++right) {
            int left = right - 1;
            if (right < equParts.size()) {
                if (equParts.get(left) instanceof OpMinus
                      && equParts.get(right) instanceof OpMinus) {
                    newList.add(new OpMinusMinus(this, equParts.get(left)));
                    right++;
                    continue;
                }
            }
            newList.add(equParts.get(left));
        }
        return newList;
    }

    private List<EquPart> negatize(List<EquPart> equParts) {
        /*
         * change subtractions to negations if necessary
         */
        int left;

        for (int right = 1; right < equParts.size(); right++) {
            left = right - 1;
            if (equParts.get(left) instanceof OpMinus) {
                if (left == 0) {
                    equParts.set(left, new OpNegate(this, equParts.get(left)));
                } else {
                    if (equParts.get(left - 1).negatize(equParts.get(right))) {
                        equParts.set(left, new OpNegate(this, equParts.get(left)));
                    }
                }
            }
        }
        return equParts;
    }

    public Operator operator(Token tok) {
        String token = tok.getValue().toString();
        Constructor<?> constructor = operatorMap.get(token);
        if (constructor == null) {
            return null;
        }
        try {
            return (Operator) constructor.newInstance(this, tok);
        } catch (Exception e) {
            throw MathException.builder().cause(new Exception("operator construction", e)).build();
        }
    }

    public void registerFunction(String name,
                                 Class<?> functionSubclass) {
        String token = name.toLowerCase();
        if (functionMap.containsKey(name)) {
            throw MathException.builder().cause(new Exception("duplicate function: " + token)).build();
        }
        try {
            functionMap.put(token, functionSubclass.getConstructor(EquImpl.class, TokVariable.class));
        } catch (Exception e) {
            throw MathException.builder().cause(new Exception("register function: " + token, e)).build();
        }
    }

    private void registerOperator(String name,
                                  Class<?> operatorSubclass) {
        String token = name.toLowerCase();
        if (operatorMap.containsKey(name)) {
            throw MathException.builder().cause(new Exception("duplicate operator: " + token)).build();
        }
        try {
            operatorMap.put(token, operatorSubclass.getConstructor(EquImpl.class, EquPart.class));
        } catch (Exception e) {
            throw MathException.builder().cause(new Exception("register operator: " + token, e)).build();
        }
    }

    private List<EquPart> rpnize(List<EquPart> oldTokens) {
        /*
         * Create a reverse Polish notation form of the equation
         */
        List<EquPart> _rpn = new Stack<>();
        Stack<EquPart> ops = new Stack<>();
        Operation leftOp;
        Operation rightOp;

        for (EquPart token : oldTokens) {
            if (token instanceof Token) {
                _rpn.add(token);
            } else {
                rightOp = (Operation) token;

                if (ops.empty()) {

                    if (rightOp.includeInRpn()) {
                        ops.push(rightOp);
                    }
                } else {
                    leftOp = (Operation) ops.peek();

                    if (leftOp.preceeds(rightOp)) {
                        _rpn.add(ops.pop());

                        /*
                         * while knocking one off scan back as long as the level
                         * doesn't change. A left paren causes the level to
                         * increase and the left paren has that new level. A
                         * right paren causes the level to decrease and has this
                         * new lesser level. So the right paren is one level
                         * less than the operators within the parens. Since we
                         * want to only scan those operators we have to adjust
                         * for the level that they are.
                         */
                        int level = rightOp.getLevel();
                        if (rightOp instanceof OpRightParen) {
                            level = level + 1;
                        }

                        Operation compareOp = rightOp;
                        while (!ops.empty()) {
                            leftOp = (Operation) ops.peek();
                            if (leftOp.getLevel() < level) {
                                break;
                            }
                            if (leftOp.preceeds(compareOp)) {
                                _rpn.add(ops.pop());
                                compareOp = leftOp;
                            } else {
                                break;
                            }
                            break;
                        }
                    }

                    if (rightOp.includeInRpn()) {
                        ops.push(rightOp);
                    }
                }
            }
        }

        while (!ops.empty()) {
            _rpn.add(ops.pop());
        }

        return _rpn;
    }

    public void setBaseDate(java.sql.Date newBaseDate) {
        baseDate = newBaseDate;
    }

    public EquImpl setSupport(EquationSupport newSupport) {
        support = newSupport;
        return this;
    }

    public String showRPN() {
        StringBuilder sb = new StringBuilder();
        showRPN(sb);
        return sb.toString();
    }

    public void showRPN(StringBuilder sb) {
        for (EquPart part : rpn) {
            sb.append(part.toString());
            sb.append("\n");
        }
    }

    private List<EquPart> tokenize() {
        try {
            /*
             * break apart obvious tokens, realizing that some may need further
             * breakage and some may need to be merged back again
             */
            List<EquPart> tokens = new ArrayList<>();
            Token token = null;
            char c;
            char peekc;
            int level = 0;

            for (int a = 0; a < equ.length(); a++) {
                c = equ.charAt(a);

                int peeka = a + 1;
                if (peeka == equ.length()) {
                    peekc = 0;
                } else {
                    peekc = equ.charAt(peeka);
                }

                boolean isWhitespace = Character.isWhitespace(c);

                if (isWhitespace) {
                    if (!(token instanceof TokLiteral && token.accepts(c, peekc))) {
                        /*
                         * spaces are allowed inside of literals
                         */
                        if (token != null) {
                            token.addTo(tokens);
                        }
                        token = null;
                        continue;
                    }
                }

                if (c == '(') {
                    level++;
                }

                if (c == ')') {
                    level--;
                }

                if (token != null && token.accepts(c, peekc)) {
                    token.put(c);
                } else {

                    if (token != null) {
                        token.addTo(tokens);
                    }

                    token = Token.instanceFor(this, c);
                    token.setLevel(level);
                    token.put(c);
                }
            }

            if (token != null) {
                token.addTo(tokens);
            }

            return tokens;
        } catch (Exception e) {
            throw MathException.builder().cause(e).build();
        }
    }

    @Override
    public String toString() {
        return equ;
    }

    public void unregisterFunction(String name) {
        String token = name.toLowerCase();
        if (!functionMap.containsKey(token)) {
            throw MathException.builder().cause(new Exception("unknown function: " + token)).build();
        }
        functionMap.remove(token);
    }

    public void unregisterOperator(String name) {
        String token = name.toLowerCase();
        if (!operatorMap.containsKey(token)) {
            throw MathException.builder().cause(new Exception("unknown operator: " + token)).build();
        }
        operatorMap.remove(token);
    }
}
