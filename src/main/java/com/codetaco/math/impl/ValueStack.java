package com.codetaco.math.impl;

import com.codetaco.date.CalendarFactory;
import com.codetaco.math.impl.token.TokVariable;
import com.codetaco.math.impl.token.TokVariableWithValue;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.Stack;

public class ValueStack extends Stack<Object> {

    public static String byteArrayAsString(Object bytearray) {
        return new String((byte[]) bytearray, StandardCharsets.ISO_8859_1);
    }

    private boolean convertToBoolean(Object fromStack) throws ParseException {

        if (fromStack instanceof TokVariableWithValue) {
            fromStack = ((TokVariableWithValue) fromStack).getCurrentValue();
        }

        if (fromStack instanceof Number) {
            /*
             * 0 is the only number that is false, all others are true.
             */
            return ((Number) fromStack).intValue() != 0;
        }
        if (fromStack instanceof String) {
            return Boolean.parseBoolean((String) fromStack);
        }
        if (fromStack instanceof Boolean) {
            return (Boolean) fromStack;
        }
        throw new ParseException("invalid type " + fromStack.getClass().getSimpleName(), 0);
    }

    public ZonedDateTime convertToZonedDateTime(Object fromStack) throws ParseException {

        if (fromStack instanceof TokVariableWithValue) {
            fromStack = ((TokVariableWithValue) fromStack).getCurrentValue();
        }

        if (fromStack instanceof ZonedDateTime) {
            return (ZonedDateTime) fromStack;
        }
        throw new ParseException("invalid type " + fromStack.getClass().getSimpleName(), 0);
    }

    public double convertToDouble(Object fromStack) throws ParseException {

        if (fromStack instanceof TokVariableWithValue) {
            fromStack = ((TokVariableWithValue) fromStack).getCurrentValue();
        }

        if (fromStack instanceof Number) {
            return ((Number) fromStack).doubleValue();
        }
        if (fromStack instanceof String) {
            return Double.parseDouble((String) fromStack);
        }
        if (fromStack instanceof ZonedDateTime) {
            return CalendarFactory.asDateLong((ZonedDateTime) fromStack);
        }
        throw new ParseException("invalid type " + fromStack.getClass().getSimpleName(), 0);
    }

    public float convertToFloat(Object fromStack) throws ParseException {

        if (fromStack instanceof TokVariableWithValue) {
            fromStack = ((TokVariableWithValue) fromStack).getCurrentValue();
        }

        if (fromStack instanceof Number) {
            return ((Number) fromStack).floatValue();
        }
        if (fromStack instanceof String) {
            return Float.parseFloat((String) fromStack);
        }
        if (fromStack instanceof ZonedDateTime) {
            return CalendarFactory.asDateLong((ZonedDateTime) fromStack);
        }
        throw new ParseException("invalid type " + fromStack.getClass().getSimpleName(), 0);
    }

    public long convertToLong(Object fromStack) throws ParseException {

        if (fromStack instanceof TokVariableWithValue) {
            fromStack = ((TokVariableWithValue) fromStack).getCurrentValue();
        }

        if (fromStack instanceof Number) {
            return ((Number) fromStack).longValue();
        }
        if (fromStack instanceof String) {
            return Long.parseLong((String) fromStack);
        }
        if (fromStack instanceof ZonedDateTime) {
            return CalendarFactory.asDateLong((ZonedDateTime) fromStack);
        }
        throw new ParseException("invalid type " + fromStack.getClass().getSimpleName(), 0);
    }

    public int convertToInteger(Object fromStack) throws ParseException {

        if (fromStack instanceof TokVariableWithValue) {
            fromStack = ((TokVariableWithValue) fromStack).getCurrentValue();
        }

        if (fromStack instanceof Number) {
            return ((Number) fromStack).intValue();
        }
        if (fromStack instanceof String) {
            return Integer.parseInt((String) fromStack);
        }
        if (fromStack instanceof ZonedDateTime) {
            return (int) (CalendarFactory.asDateLong((ZonedDateTime) fromStack));
        }
        throw new ParseException("invalid type " + fromStack.getClass().getSimpleName(), 0);
    }

    public Object[] ensureSameTypes() throws ParseException {
        return ensureSameTypes(2);
    }

    public Object[] ensureSameTypes(int count) throws ParseException {
        Object[] ops = new Object[count];
        for (int p = 0; p < count; p++) {
            ops[p] = popWhatever();
        }
        return ensureSameTypes(ops);
    }

    public Object[] ensureSameTypes(Object... fromStack) throws ParseException {
        boolean foundDate = false;
        boolean foundString = false;
        boolean foundInteger = false;
        boolean foundFloat = false;
        boolean foundLong = false;
        boolean foundDouble = false;
        boolean foundAllSame = true;

        Object firstFound = null;
        Stack<Object> ops = new Stack<>();
        for (int p = 0; p < fromStack.length; p++) {
            Object currentFound = fromStack[p];

            if (currentFound instanceof TokVariable) {
                throw new ParseException(
                  "\"" + ((TokVariable) currentFound).getName() + "\" is unassigned", 0);
            }

            if (currentFound instanceof TokVariableWithValue) {
                currentFound = ((TokVariableWithValue) currentFound).getCurrentValue();
            }

            ops.push(currentFound);
            if (firstFound == null) {
                firstFound = currentFound;
            }
            if (!firstFound.getClass().equals(currentFound.getClass())) {
                foundAllSame = false;
            }
            if (currentFound instanceof Long) {
                foundLong = true;
            } else if (currentFound instanceof Double) {
                foundDouble = true;
            } else if (currentFound instanceof Integer) {
                foundInteger = true;
            } else if (currentFound instanceof Float) {
                foundFloat = true;
            } else if (currentFound instanceof String) {
                foundString = true;
            } else if (currentFound instanceof ZonedDateTime) {
                foundDate = true;
            }
        }
        Object[] found = new Object[ops.size()];
        for (int x = ops.size() - 1; x >= 0; x--) {
            Object oneFound = ops.pop();
            if (foundAllSame) {
                found[x] = oneFound;
            } else if (foundDouble) {
                found[x] = convertToDouble(oneFound);
            } else if (foundFloat) {
                found[x] = convertToFloat(oneFound);
            } else if (foundLong) {
                found[x] = convertToLong(oneFound);
            } else if (foundInteger) {
                found[x] = convertToInteger(oneFound);
            } else if (foundString) {
                found[x] = oneFound.toString();
            } else {

                StringBuilder foundTypes = new StringBuilder();
                foundTypes.append("invalid mixed types: ");

                if (foundDate) {
                    foundTypes.append("ZonedDateTime ");
                } else if (foundDouble) {
                    foundTypes.append("Double ");
                } else if (foundFloat) {
                    foundTypes.append("Float ");
                } else if (foundLong) {
                    foundTypes.append("Long ");
                } else if (foundInteger) {
                    foundTypes.append("Integer ");
                } else if (foundString) {
                    foundTypes.append("String ");
                }
                foundTypes.setLength(foundTypes.length() - 1);
                throw new ParseException(foundTypes.toString(), 0);
            }
        }
        return found;
    }

    public boolean popBoolean() throws ParseException {
        Object popped = super.pop();
        if (popped instanceof TokVariableWithValue) {
            popped = ((TokVariableWithValue) popped).getCurrentValue();
        }
        return convertToBoolean(popped);
    }

    public byte[] popByteArray() throws ParseException {
        Object popped = super.pop();
        if (popped instanceof TokVariableWithValue) {
            popped = ((TokVariableWithValue) popped).getCurrentValue();
        }
        if (popped instanceof byte[]) {
            return (byte[]) popped;
        }

        throw new ParseException("byte[] required, found " + popped.getClass().getSimpleName(), 0);
    }

    public ZonedDateTime popZonedDateTime() throws ParseException {
        Object o = super.pop();
        if (o instanceof TokVariableWithValue) {
            o = ((TokVariableWithValue) o).getCurrentValue();
        }
        return convertToZonedDateTime(o);
    }

    public double popDouble() throws ParseException {
        Object o = super.pop();
        if (o instanceof TokVariableWithValue) {
            o = ((TokVariableWithValue) o).getCurrentValue();
        }
        return convertToDouble(o);
    }

    public float popFloat() throws ParseException {
        Object o = super.pop();
        if (o instanceof TokVariableWithValue) {
            o = ((TokVariableWithValue) o).getCurrentValue();
        }
        return convertToFloat(o);
    }

    public long popLong() throws ParseException {
        Object o = super.pop();
        if (o instanceof TokVariableWithValue) {
            o = ((TokVariableWithValue) o).getCurrentValue();
        }
        return convertToLong(o);
    }

    public int popInteger() throws ParseException {
        Object o = super.pop();
        if (o instanceof TokVariableWithValue) {
            o = ((TokVariableWithValue) o).getCurrentValue();
        }
        return convertToInteger(o);
    }

    public String popString() throws ParseException {
        Object popped = super.pop();
        if (popped instanceof String) {
            return (String) popped;
        }
        /*
         * This is probably an unquoted single word literal.
         */
        if (popped instanceof TokVariable) {
            return ((TokVariable) popped).getName();
        }
        if (popped instanceof TokVariableWithValue) {
            return (String) ((TokVariableWithValue) popped).getCurrentValue();
        }

        throw new ParseException("Literal required, found " + popped.getClass().getSimpleName(), 0);
    }

    public Object popStringOrByteArray() throws ParseException {
        Object popped = super.pop();

        if (popped instanceof TokVariableWithValue) {
            popped = ((TokVariableWithValue) popped).getCurrentValue();
        }
        /*
         * This is probably an unquoted single word literal.
         */
        if (popped instanceof TokVariable) {
            return ((TokVariable) popped).getName();
        }

        if (popped instanceof String || popped instanceof byte[]) {
            return popped;
        }

        throw new ParseException("Literal or byte[] required, found " + popped.getClass().getSimpleName(), 0);
    }

    public Object popWhatever() {
        Object o = super.pop();
        if (o instanceof TokVariableWithValue) {
            return ((TokVariableWithValue) o).getCurrentValue();
        }
        return o;
    }

    public Object popExactly() {
        return super.pop();
    }
}
