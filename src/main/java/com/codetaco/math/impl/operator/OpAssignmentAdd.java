package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operation;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;
import com.codetaco.math.impl.token.TokVariableWithValue;

import java.text.ParseException;

public class OpAssignmentAdd extends Operator {
    public OpAssignmentAdd(EquImpl equ) {
        super(equ);
    }

    public OpAssignmentAdd(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 990;
    }

    @Override
    public boolean preceeds(Operation rightOp) {
        /*
         * This allows for assigning the same value to multiple variables. <br>
         * a := b := 1
         */
        if (rightOp instanceof OpAssignmentAdd) {
            return false;
        }

        return super.preceeds(rightOp);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            Object op2 = values.popExactly();
            Object op1 = values.popExactly();

            if (op2 instanceof TokVariable) {
                throw new Exception("invalid assignment value: " + op2.toString());
            }
            if (!(op1 instanceof TokVariableWithValue)) {
                throw new Exception("invalid assignment target: " + op1.toString());
            }

            Object[] fromStack = values.ensureSameTypes(op1, op2);
            TokVariableWithValue varWithValue1 = (TokVariableWithValue) op1;

            if (fromStack[0] instanceof Integer) {
                varWithValue1.setCurrentValue((Integer) fromStack[0] + (Integer) fromStack[1]);
            } else if (fromStack[0] instanceof Long) {
                varWithValue1.setCurrentValue((Long) fromStack[0] + (Long) fromStack[1]);
            } else if (fromStack[0] instanceof Double) {
                varWithValue1.setCurrentValue((Double) fromStack[0] + (Double) fromStack[1]);
            } else if (fromStack[0] instanceof Float) {
                varWithValue1.setCurrentValue((Float) fromStack[0] + (Float) fromStack[1]);
            } else {
                throw new Exception("invalid types");
            }
            /*
             * Assign the value on the right side of the assignment to the
             * variable on the left.
             */
            getEqu().getSupport().assignVariable(
              varWithValue1.getVariable().getValue().toString(),
              varWithValue1);
            /*
             * leave the result (value of the assignment) on the stack.
             */
            values.push(varWithValue1);

        } catch (ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(+=)";
    }
}
