package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

import java.text.ParseException;

public class OpCompareEqual extends Operator {

    public OpCompareEqual(EquImpl equ) {
        super(equ);
    }

    public OpCompareEqual(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 8;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object[] ops = values.ensureSameTypes();
            values.push(new Boolean(ops[1].equals(ops[0])));
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }

        return;
    }

    @Override
    public String toString() {
        return "op(compare equal)";
    }
}
