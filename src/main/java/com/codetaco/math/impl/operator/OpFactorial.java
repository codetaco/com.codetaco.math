package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

import java.text.ParseException;

public class OpFactorial extends Operator {
    public OpFactorial(EquImpl equ) {
        super(equ);
    }

    public OpFactorial(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    public boolean multiplize(final EquPart rightSide) {
        return !(rightSide instanceof Operator);
    }

    @Override
    public boolean negatize(final EquPart rightSide) {
        return false;
    }

    @Override
    protected int precedence() {
        return 3;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 1) {
            throw new Exception("missing operand for " + toString());
        }
        try {
            final long base = values.popLong();

            if (base > 20) {
                throw new Exception(toString() + "; " + "numeric overflow");
            }

            if (base < 0) {
                throw new Exception(toString() + "; " + "negative numbers not allowed");
            }

            long factorial = 1;
            for (int i = (int) base; i > 1; i--) {
                factorial *= i;
            }

            values.push(new Long(factorial));
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(factorial)";
    }
}
