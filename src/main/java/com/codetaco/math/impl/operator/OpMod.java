package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

import java.text.ParseException;

public class OpMod extends Operator {
    public OpMod(EquImpl equ) {
        super(equ);
    }

    public OpMod(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 3;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object[] value = values.ensureSameTypes();
            if (value[0] instanceof Long) {
                values.push(new Long((Long) value[1] % (Long) value[0]));
            } else {
                values.push(new Double((Double) value[1] % (Double) value[0]));
            }

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(mod)";
    }
}
