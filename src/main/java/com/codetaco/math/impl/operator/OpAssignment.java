package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operation;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;
import com.codetaco.math.impl.token.TokVariableWithValue;

import java.text.ParseException;

public class OpAssignment extends Operator {
    public OpAssignment(EquImpl equ) {
        super(equ);
    }

    public OpAssignment(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 990;
    }

    @Override
    public boolean preceeds(final Operation rightOp) {
        /*
         * This allows for assigning the same value to multiple variables. <br>
         * a := b := 1
         */
        if (rightOp instanceof OpAssignment) {
            return false;
        }

        return super.preceeds(rightOp);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object op2 = values.popExactly();
            final Object op1 = values.popExactly();

            if (op2 instanceof TokVariable) {
                throw new Exception("invalid assignment value: " + op2.toString());
            }

            if (op1 instanceof TokVariable) {
                /*
                 * Assign the value on the right side of the assignment to the
                 * variable on the left.
                 */
                getEqu().getSupport().assignVariable(((TokVariable) op1).getValue().toString(), op2);
                /*
                 * leave the result (value of the assignment) on the stack.
                 */
                values.push(op2);
                return;
            }

            if (op1 instanceof TokVariableWithValue) {
                TokVariableWithValue varValue1 = (TokVariableWithValue) op1;
                /*
                 * Assign the value on the right side of the assignment to the
                 * variable on the left.
                 */
                getEqu().getSupport().assignVariable(varValue1.getVariable().getValue().toString(), op2);
                /*
                 * leave the result (value of the assignment) on the stack.
                 */
                values.push(op2);
                return;
            }

            throw new Exception("invalid assignment target: " + op1.toString());

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(assignment)";
    }
}
