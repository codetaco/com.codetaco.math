package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokOperand;

public class OpRightParen extends Operator {
    public OpRightParen(EquImpl equ) {
        super(equ);
    }

    public OpRightParen(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    public boolean includeInRpn() {
        return false;
    }

    @Override
    public boolean multiplize(final EquPart rightSide) {
        return rightSide instanceof OpLeftParen || rightSide instanceof TokOperand || rightSide instanceof Function;
    }

    @Override
    public boolean negatize(final EquPart rightSide) {
        return false;
    }

    @Override
    protected int precedence() {
        return 999;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        throw new Exception("WHAT! " + toString());
    }

    @Override
    public String toString() {
        return "op(closeparen)";
    }
}
