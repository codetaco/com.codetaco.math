package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

abstract public class OpEquals extends Operator {
    public OpEquals(EquImpl equ) {
        super(equ);
    }

    public OpEquals(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 999;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() != 1) {
            throw new Exception("Wrong number of operands " + toString());
        }
    }

    @Override
    public String toString() {
        return "op(assignment)";
    }
}
