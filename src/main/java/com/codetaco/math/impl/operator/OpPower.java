package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

import java.text.ParseException;

public class OpPower extends Operator {
    public OpPower(EquImpl equ) {
        super(equ);
    }

    public OpPower(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 3;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object[] value = values.ensureSameTypes();
            if (value[0] instanceof Long) {
                values.push(new Long((long) Math.pow((Long) value[1], (Long) value[0])));
            } else {
                values.push(new Double(Math.pow((Double) value[1], (Double) value[0])));
            }

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(power)";
    }
}
