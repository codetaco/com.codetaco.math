package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operation;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;
import com.codetaco.math.impl.token.TokVariableWithValue;

import java.text.ParseException;

public class OpPlusPlus extends Operator {
    public OpPlusPlus(EquImpl equ) {
        super(equ);
    }

    public OpPlusPlus(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 990;
    }

    @Override
    public boolean preceeds(final Operation rightOp) {
        /*
         * This allows for assigning the same value to multiple variables. <br>
         * a := b := 1
         */
        if (rightOp instanceof OpPlusPlus) {
            return false;
        }

        return super.preceeds(rightOp);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 1) {
            throw new Exception("missing operand for " + toString());
        }
        try {
            final Object op1 = values.popExactly();

            if (op1 instanceof TokVariable) {
                throw new Exception("invalid assignment value: " + op1.toString());
            }

            if (op1 instanceof TokVariableWithValue) {

                TokVariableWithValue varWithValue1 = (TokVariableWithValue) op1;
                if (varWithValue1.getCurrentValue() instanceof Long) {
                    varWithValue1.setCurrentValue(((Long) varWithValue1.getCurrentValue()) + 1L);
                } else if (varWithValue1.getCurrentValue() instanceof Double) {
                    varWithValue1.setCurrentValue(((Double) varWithValue1.getCurrentValue()) + 1D);
                } else {
                    throw new Exception("invalid types");
                }
                /*
                 * Assign the value on the right side of the assignment to the
                 * variable on the left.
                 */
                getEqu().getSupport().assignVariable(
                  varWithValue1.getVariable().getValue().toString(),
                  varWithValue1);
                /*
                 * leave the result (value of the assignment) on the stack.
                 */
                values.push(varWithValue1);
                return;
            }
            if (op1 instanceof Long) {
                values.push((Long) op1 + 1L);
            } else if (op1 instanceof Double) {
                values.push((Double) op1 + 1D);
            } else {
                throw new Exception("invalid type for operation: " + op1.toString());
            }

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(postfix++)";
    }
}
