package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

import java.text.ParseException;

public class OpNegate extends Operator {
    public OpNegate(EquImpl equ) {
        super(equ);
    }

    public OpNegate(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 3;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 1) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object value = values.popWhatever();
            if (value instanceof Long) {
                values.push(new Long(0L - (Long) value));
            } else if (value instanceof Double) {
                values.push(new Double(0.0 - (Double) value));
            } else {
                throw new Exception(toString() + "; invalid type: " + value.toString());
            }
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(negate)";
    }
}
