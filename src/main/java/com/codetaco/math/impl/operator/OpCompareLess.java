package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

import java.text.ParseException;
import java.time.ZonedDateTime;

public class OpCompareLess extends Operator {

    public OpCompareLess(EquImpl equ) {
        super(equ);
    }

    public OpCompareLess(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 8;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object[] ops = values.ensureSameTypes();
            if (ops[0] instanceof ZonedDateTime) {
                values.push(((ZonedDateTime) ops[1]).compareTo((ZonedDateTime) ops[0]) < 0);
                return;
            }
            if (ops[0] instanceof Double) {
                values.push((Double) ops[1] < (Double) ops[0]);
                return;
            }
            if (ops[0] instanceof Long) {
                values.push((Long) ops[1] < (Long) ops[0]);
                return;
            }
            throw new Exception(toString() + "; invalid type, found " + ops[0].getClass().getSimpleName());
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(compare less)";
    }
}
