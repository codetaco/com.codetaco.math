package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

import java.text.ParseException;

public class OpAnd extends Operator {
    public OpAnd(EquImpl equ) {
        super(equ);
    }

    public OpAnd(EquImpl equ, final EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 9;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final boolean b0 = values.popBoolean();
            final boolean b1 = values.popBoolean();
            values.push(new Boolean(b1 && b0));
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(and)";
    }
}
