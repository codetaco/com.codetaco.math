package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.ValueStack;

public class OpCompareNotGreater extends OpCompareGreater {

    public OpCompareNotGreater(EquImpl equ) {
        super(equ);
    }

    public OpCompareNotGreater(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        super.resolve(values);
        values.push(!values.popBoolean());
    }

    @Override
    public String toString() {
        return "op(compare not greater)";
    }
}
