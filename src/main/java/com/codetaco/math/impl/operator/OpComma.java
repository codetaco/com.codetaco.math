package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

public class OpComma extends Operator {
    public OpComma(EquImpl equ) {
        super(equ);
    }

    public OpComma(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    public boolean includeInRpn() {
        return false;
    }

    @Override
    protected int precedence() {
        return 999;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        throw new Exception("WHAT! " + toString());
    }

    @Override
    public String toString() {
        return "op(comma)";
    }
}
