package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

import java.text.ParseException;

public class OpChain extends Operator {

    public OpChain(EquImpl equ) {
        super(equ);
    }

    public OpChain(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    public boolean includeInRpn() {
        return true;
    }

    @Override
    public boolean negatize(final EquPart rightSide) {
        return true;
    }

    @Override
    protected int precedence() {
        return 999;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new ParseException("missing operand", 0);
        }

        final Object rightSideResult = values.popWhatever();
        values.popWhatever();

        values.push(rightSideResult);
    }

    @Override
    public String toString() {
        return "op(chain)";
    }
}
