package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

import java.text.ParseException;

public class OpDivide extends Operator {
    public OpDivide(EquImpl equ) {
        super(equ);
    }

    public OpDivide(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 4;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object[] value = values.ensureSameTypes();
            double cop1 = values.convertToDouble(value[1]);
            double cop0 = values.convertToDouble(value[0]);
            values.push(cop1 / cop0);
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(divide)";
    }
}
