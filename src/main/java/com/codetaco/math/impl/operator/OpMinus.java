package com.codetaco.math.impl.operator;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Operator;
import com.codetaco.math.impl.ValueStack;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class OpMinus extends Operator {
    public OpMinus(EquImpl equ) {
        super(equ);
    }

    public OpMinus(EquImpl equ, EquPart opTok) {
        super(equ, opTok);
    }

    @Override
    protected int precedence() {
        return 6;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object[] value = values.ensureSameTypes();
            if (value[0] instanceof ZonedDateTime) {
                final ZonedDateTime ldt0 = (ZonedDateTime) value[0];
                final ZonedDateTime ldt1 = (ZonedDateTime) value[1];
                values.push(ChronoUnit.DAYS.between(ldt0, ldt1));

            } else if (value[0] instanceof Integer) {
                values.push((Integer) value[1] - (Integer) value[0]);
            } else if (value[0] instanceof Long) {
                values.push((Long) value[1] - (Long) value[0]);
            } else if (value[0] instanceof Float) {
                values.push((Float) value[1] - (Float) value[0]);
            } else if (value[0] instanceof Double) {
                values.push((Double) value[1] - (Double) value[0]);
            } else {
                throw new Exception("invalid types");
            }

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "op(minus)";
    }
}
