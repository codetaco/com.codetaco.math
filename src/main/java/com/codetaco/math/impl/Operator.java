package com.codetaco.math.impl;

public abstract class Operator extends Operation {
    public Operator(EquImpl equ) {
        super(equ);
    }

    public Operator(EquImpl equ, EquPart opTok) {
        super(equ);
        setLevel(opTok.getLevel());
    }

    @Override
    public boolean negatize(final EquPart rightSide) {
        return true;
    }

    @Override
    protected int precedence() {
        return 999;
    }

}
