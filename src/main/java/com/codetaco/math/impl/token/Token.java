package com.codetaco.math.impl.token;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;

import java.util.Collection;

public abstract class Token extends EquPart {

    public static Token instanceFor(EquImpl equ, final char c) {
        if (c == '\'' || c == '"') {
            return new TokLiteral(equ, c);
        }

        if (Character.isDigit(c) || c == '-' || c == '.') {
            return new TokNumber(equ);
        }

        if (Character.isLetter(c) || c == '_') {
            return new TokVariable(equ);
        }

        return new TokOperator(equ);
    }

    private StringBuffer value;

    public Token(EquImpl equ) {
        super(equ);
        setValue(new StringBuffer());
    }

    public boolean accepts(final char c) {
        return true;
    }

    public boolean accepts(final char c, char peek) {
        /*
        override this if you need to peek
         */
        return accepts(c);
    }

    public void addTo(final Collection<EquPart> tokens) throws Exception {
        tokens.add(morph());
    }

    public java.lang.StringBuffer getValue() {
        return value;
    }

    protected EquPart morph() throws Exception {
        return this;
    }

    public void put(final char c) {
        getValue().append(c);
    }

    public void setValue(final java.lang.StringBuffer newValue) {
        value = newValue;
    }

    @Override
    public String toString() {
        return getValue().toString();
    }
}
