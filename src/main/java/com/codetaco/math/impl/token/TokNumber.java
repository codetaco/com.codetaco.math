package com.codetaco.math.impl.token;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.ValueStack;

public class TokNumber extends TokOperand {

    public TokNumber(EquImpl equ) {
        super(equ);
    }

    @Override
    public boolean accepts(char s, char peek) {
        if (Character.isDigit(s) || s == '.') {
            return true;
        }
        if (getValue().length() == 0) {
            return false;
        }
        if (s == '=') {
            return getValue().length() == 1 && getValue().charAt(0) == '-';
        }
        char prevChar = getValue().charAt(getValue().length() - 1);
        if (s == '-') {
            return getValue().length() > 1 && (prevChar == 'e' || prevChar == 'E');
        }
        if (s == 'e' || s == 'E') {
            if (getValue().indexOf("e") > -1 || getValue().indexOf("E") > -1) {
                return false;
            }
            return Character.isDigit(prevChar) && (Character.isDigit(peek) || peek == '-');
        }
        return false;
    }

    @Override
    public EquPart morph() {
        if (getValue().length() == 1 && getValue().charAt(0) == '-') {
            EquPart part = getEqu().operator(this);
            if (part != null) {
                return part;
            }
        }
        if (getValue().length() == 2 && getValue().charAt(0) == '-' && getValue().charAt(1) == '=') {
            EquPart part = getEqu().operator(this);
            if (part != null) {
                return part;
            }
        }
        return this;
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        String valueToBeParsed = getValue().toString();
        if (valueToBeParsed.indexOf('.') == -1
              && valueToBeParsed.indexOf('e') == -1
              && valueToBeParsed.indexOf('E') == -1) {
            values.push(new Long(valueToBeParsed));
        } else {
            values.push(new Double(valueToBeParsed));
        }
    }

    @Override
    public String toString() {
        return "num(" + super.toString() + ")";
    }
}
