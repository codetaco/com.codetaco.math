package com.codetaco.math.impl.token;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.operator.OpLeftParen;

public abstract class TokOperand extends Token {
    public TokOperand(EquImpl equ) {
        super(equ);
    }

    @Override
    public boolean multiplize(final EquPart rightSide) {
        return (rightSide instanceof OpLeftParen || rightSide instanceof TokOperand || rightSide instanceof Function);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        values.push(getValue());
    }
}
