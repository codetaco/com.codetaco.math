package com.codetaco.math.impl.token;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.EquPart;
import com.codetaco.math.impl.ValueStack;

public class TokOperator extends Token {
    public TokOperator(EquImpl equ) {
        super(equ);
    }

    @Override
    public boolean accepts(final char s) {
        /*
         * possible second characters in a double (or more) character symbol
         */
        switch (s) {
            case '=':
                return true;
            case '&':
                return true;
            case '|':
                return true;
            case '~':
                return true;
            default:
                break;
        }
        if (getValue().length() == 1 && getValue().charAt(0) == '+' && s == '+') {
            return true;
        }
        return (getValue().length() == 1 && getValue().charAt(0) == '-' && s == '-');
    }

    @Override
    public EquPart morph() throws Exception {
        final EquPart part = getEqu().operator(this);
        if (part == null) {
            return this;
        }
        return part;
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        throw new Exception("Unknown token encountered in equation: " + getValue().toString());
    }

    @Override
    public String toString() {
        return "nonop(" + super.toString() + ")";
    }
}
