package com.codetaco.math.impl.token;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.ValueStack;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TokVariableWithValue extends TokOperand {

    TokVariable variable;
    Object currentValue;

    public TokVariableWithValue(EquImpl equ) {
        super(equ);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (getEqu().getSupport() == null) {
            throw new Exception("variables require support");
        }

    }
}