package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FuncStringReplace extends Function {
    private static void pushAppropriateType(ValueStack values,
                                            Object whoKnowsWhat,
                                            String result) {
        if (whoKnowsWhat instanceof String) {
            values.push(result);

        } else if (whoKnowsWhat instanceof byte[]) {
            values.push((result.getBytes(StandardCharsets.ISO_8859_1)));
        }
    }

    public FuncStringReplace(EquImpl equ) {
        super(equ);
    }

    public FuncStringReplace(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        if (getParameterCount() != 3) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            String target;
            String pattern;
            String replacement = values.popString();
            pattern = values.popString();

            Object whoKnowsWhat = values.popStringOrByteArray();
            if (whoKnowsWhat instanceof String) {
                target = (String) whoKnowsWhat;

            } else {
                target = ValueStack.byteArrayAsString(whoKnowsWhat);
            }

            Matcher matcher = Pattern.compile(pattern).matcher(target);
            pushAppropriateType(values, whoKnowsWhat, matcher.replaceAll(replacement));

        } catch (ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(replace)";
    }
}
