package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;
import java.util.regex.Pattern;

public class FuncStringContains extends Function {

    public FuncStringContains(EquImpl equ) {
        super(equ);
    }

    public FuncStringContains(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            String target;
            String pattern = values.popString();

            Object whoKnowsWhat = values.popStringOrByteArray();
            if (whoKnowsWhat instanceof String) {
                target = (String) whoKnowsWhat;
            } else {
                target = ValueStack.byteArrayAsString(whoKnowsWhat);
            }
            values.push(Pattern.compile(pattern).matcher(target).find());

        } catch (ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(contains-" + getParameterCount() + ")";
    }
}
