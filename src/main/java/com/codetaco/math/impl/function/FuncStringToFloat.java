package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

public class FuncStringToFloat extends Function {

    public FuncStringToFloat(EquImpl equ) {
        super(equ);
    }

    public FuncStringToFloat(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object value = values.popWhatever();
            if (value instanceof String) {
                if (((String) value).trim().length() == 0) {
                    values.push(new Double(0));
                } else {
                    values.push(Double.parseDouble((String) value));
                }
            } else if (value instanceof Long) {
                values.push(new Double((Long) value));
            } else if (value instanceof Double) {
                values.push(value);
            }
        } catch (final NumberFormatException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(toFloat)";
    }
}
