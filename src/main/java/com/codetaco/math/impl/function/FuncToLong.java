package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncToLong extends AbstractToNumber {
    public FuncToLong(EquImpl equ) {
        super(equ);
    }

    public FuncToLong(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    protected Object popSomething(ValueStack values) throws ParseException {
        return values.popLong();
    }

    @Override
    public String toString() {
        return "function(toLong)";
    }
}
