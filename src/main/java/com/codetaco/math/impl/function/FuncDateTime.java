package com.codetaco.math.impl.function;

import com.codetaco.date.CalendarFactory;
import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;
import java.time.ZonedDateTime;

public class FuncDateTime extends Function {

    public FuncDateTime(EquImpl equ) {
        super(equ);
    }

    public FuncDateTime(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }

        try {
            String adjustments = values.popString();
            ZonedDateTime inputDate = values.popZonedDateTime();
            values.push(CalendarFactory.asZoned(inputDate, adjustments));

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(datetime)";
    }
}
