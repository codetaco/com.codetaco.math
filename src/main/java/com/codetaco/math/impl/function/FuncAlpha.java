package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;
import java.util.Formatter;

public class FuncAlpha extends Function {
    public FuncAlpha(EquImpl equ) {
        super(equ);
    }

    public FuncAlpha(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < 1) {
            throw new Exception("missing operands for " + toString());
        }

        try {
            StringBuilder buf = new StringBuilder();
            try (Formatter fmt = new Formatter(buf)) {
                fmt.format("%d", (int) values.popDouble());
            }
            values.push(buf.toString());
        } catch (ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(alpha)";
    }
}
