package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;
import java.util.regex.Pattern;

public class FuncStringMatches extends Function {

    public FuncStringMatches(EquImpl equ) {
        super(equ);
    }

    public FuncStringMatches(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            String target;
            String pattern = values.popString();

            final Object whoKnowsWhat = values.popStringOrByteArray();
            if (whoKnowsWhat instanceof String) {
                target = (String) whoKnowsWhat;
            } else {
                target = ValueStack.byteArrayAsString(whoKnowsWhat);
            }
            values.push(Pattern.compile(pattern).matcher(target).matches());

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(matches-" + getParameterCount() + ")";
    }
}
