package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

public class FuncStringToInt extends Function {
    public FuncStringToInt(EquImpl equ) {
        super(equ);
    }

    public FuncStringToInt(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object value = values.popWhatever();
            if (value instanceof String) {
                if (((String) value).trim().length() == 0) {
                    values.push(new Long(0));
                } else {
                    values.push(Long.parseLong((String) value));
                }
                return;

            } else if (value instanceof Double) {
                values.push(new Long(((Double) value).longValue()));
                return;

            } else if (value instanceof Long) {
                values.push(value);
                return;
            }
            values.push(value);
        } catch (final NumberFormatException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(toInt)";
    }
}
