package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;
import java.util.Arrays;

public class FuncStringSubstr extends Function {
    public FuncStringSubstr(EquImpl equ) {
        super(equ);
    }

    public FuncStringSubstr(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 3) {
            throw new Exception("requires 3 parameters " + toString());
        }
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Double length = values.popDouble();
            final Double offset = values.popDouble();
            final Object target = values.popStringOrByteArray();

            if (target instanceof String) {
                values.push(((String) target).substring(
                  offset.intValue(), offset.intValue() + length.intValue()));
            } else {
                values.push(Arrays.copyOfRange((byte[]) target,
                                               offset.intValue(), offset.intValue() + length.intValue()));
            }

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(substr)";
    }
}
