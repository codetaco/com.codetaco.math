package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class FuncToDateTime extends AbstractToTemporal {

    public FuncToDateTime(EquImpl equ) {
        super(equ);
    }

    public FuncToDateTime(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    void parseWithUserSuppliedFormat(ValueStack values,
                                     String userSuppliedFormatString,
                                     String inputObject) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(userSuppliedFormatString);
        LocalDateTime localDateTime = LocalDateTime.parse(inputObject, dtf);
        values.push(ZonedDateTime.of(localDateTime, ZoneOffset.UTC));
    }

    @Override
    public String toString() {
        return "function(toDateTime)";
    }

    @Override
    String adjustments() {
        return "";
    }
}
