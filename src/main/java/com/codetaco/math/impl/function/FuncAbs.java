package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

public class FuncAbs extends Function {

    public FuncAbs(EquImpl equ) {
        super(equ);
    }

    public FuncAbs(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 1) {
            throw new Exception("missing operands for " + toString());
        }
        final double data = values.popDouble();
        values.push(new Double(Math.abs(data)));
    }

    @Override
    public String toString() {
        return "function(abs)";
    }
}
