package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncStringLength extends Function {

    public FuncStringLength(EquImpl equ) {
        super(equ);
    }

    public FuncStringLength(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object target = values.popStringOrByteArray();

            if (target instanceof String) {
                values.push(new Double(((String) target).length()));
            } else {
                values.push(new Double(((byte[]) target).length));
            }

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(length)";
    }
}
