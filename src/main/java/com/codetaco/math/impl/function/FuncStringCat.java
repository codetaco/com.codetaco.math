package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncStringCat extends Function {
    public FuncStringCat(EquImpl equ) {
        super(equ);
    }

    public FuncStringCat(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }

        try {
            final StringBuilder sb = new StringBuilder();
            for (int p = 0; p < getParameterCount(); p++) {
                sb.insert(0, values.popString());
            }

            values.push(sb.toString());
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(cat)";
    }
}
