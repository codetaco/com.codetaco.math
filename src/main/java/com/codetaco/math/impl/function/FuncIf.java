package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

public class FuncIf extends Function {
    public FuncIf(EquImpl equ) {
        super(equ);
    }

    public FuncIf(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < 3) {
            throw new Exception("missing operands for " + toString());
        }

        Object op3 = values.popWhatever();
        Object op2 = values.popWhatever();
        boolean op1 = values.popBoolean();

        values.push(op1
                      ? op2
                      : op3);
    }

    @Override
    public String toString() {
        return "function(if)";
    }
}
