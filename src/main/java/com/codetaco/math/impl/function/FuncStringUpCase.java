package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncStringUpCase extends Function {
    public FuncStringUpCase(EquImpl equ) {
        super(equ);
    }

    public FuncStringUpCase(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            String target = values.popString();

            values.push(target.toUpperCase());
        } catch (ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(ucase)";
    }
}
