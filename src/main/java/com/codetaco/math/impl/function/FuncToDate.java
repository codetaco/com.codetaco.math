package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class FuncToDate extends AbstractToTemporal {

    public FuncToDate(EquImpl equ) {
        super(equ);
    }

    public FuncToDate(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    void parseWithUserSuppliedFormat(ValueStack values,
                                     String userSuppliedFormatString,
                                     String inputObject) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(userSuppliedFormatString);
        LocalDate localDate = LocalDate.parse(inputObject, dtf);
        values.push(ZonedDateTime.of(localDate, LocalTime.MIN, ZoneOffset.UTC));
    }

    @Override
    String adjustments() {
        return "=btime";
    }

    @Override
    public String toString() {
        return "function(toDate)";
    }
}
