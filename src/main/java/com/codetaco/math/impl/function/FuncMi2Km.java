package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

public class FuncMi2Km extends Function {
    public FuncMi2Km(EquImpl equ) {
        super(equ);
    }

    public FuncMi2Km(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 1) {
            throw new Exception("missing operand for " + toString());
        }

        Double kilometersPerMile = 1.609344;
        Double miles = values.popDouble();

        values.push(miles * kilometersPerMile);
    }

    @Override
    public String toString() {
        return "function(mi2km)";
    }
}
