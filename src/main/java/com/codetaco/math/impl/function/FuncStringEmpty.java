package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncStringEmpty extends Function {
    public FuncStringEmpty(EquImpl equ) {
        super(equ);
    }

    public FuncStringEmpty(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            boolean result;
            final Object whoKnowsWhat = values.popStringOrByteArray();
            if (whoKnowsWhat instanceof String) {
                final String target = (String) whoKnowsWhat;
                result = target.trim().length() == 0;

            } else {
                final byte[] target = (byte[]) whoKnowsWhat;
                result = target == null || target.length == 0;
            }

            values.push(new Boolean(result));

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(empty)";
    }
}
