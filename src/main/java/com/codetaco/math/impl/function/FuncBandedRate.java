package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.support.EquationSupport;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;
import java.util.Enumeration;
import java.util.Hashtable;

public class FuncBandedRate extends Function {
    public FuncBandedRate(EquImpl equ) {
        super(equ);
    }

    public FuncBandedRate(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }

        try {
            String tableName = values.popString();
            double baseAmount = values.popDouble();
            EquationSupport model = getEqu().getSupport();

            Hashtable<Double, Double> rateTable = model
                                                          .resolveRate(tableName, getEqu().getBaseDate(), baseAmount);
            double blendedRate = 0;

            /*
             * only the last key in the table is used for banded rates
             */
            for (Enumeration<Double> limits = rateTable.keys(); limits.hasMoreElements(); ) {
                Double limit = limits.nextElement();
                blendedRate = (rateTable.get(limit)).doubleValue();
            }

            values.push(new Double(blendedRate));
        } catch (ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(bandedrate)";
    }
}
