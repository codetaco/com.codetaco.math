package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FuncStringMatch extends Function {

    private void pushAppropriateType(final ValueStack values,
                                     final Object whoKnowsWhat,
                                     final String result) {
        if (whoKnowsWhat instanceof String) {
            values.push(result);

        } else if (whoKnowsWhat instanceof byte[]) {
            values.push((result.getBytes(StandardCharsets.ISO_8859_1)));
        }
    }

    public FuncStringMatch(EquImpl equ) {
        super(equ);
    }

    public FuncStringMatch(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            String target;
            String pattern;
            int groupNumber = 0;
            if (getParameterCount() == 3) {
                groupNumber = (int) values.popLong();
            }
            pattern = values.popString();

            final Object whoKnowsWhat = values.popStringOrByteArray();
            if (whoKnowsWhat instanceof String) {
                target = (String) whoKnowsWhat;
            } else {
                target = ValueStack.byteArrayAsString(whoKnowsWhat);
            }

            final Matcher matcher = Pattern.compile(pattern).matcher(target);

            if (matcher.find()) {
                pushAppropriateType(values, whoKnowsWhat, matcher.group(groupNumber));
            } else {
                pushAppropriateType(values, whoKnowsWhat, "");
            }

        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(match-" + getParameterCount() + ")";
    }
}
