package com.codetaco.math.impl.function;

import com.codetaco.date.CalendarFactory;
import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;
import java.time.ZonedDateTime;

public abstract class AbstractToTemporal extends Function {

    AbstractToTemporal(EquImpl equ) {
        super(equ);
    }

    AbstractToTemporal(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < 1) {
            throw new Exception("missing operands for " + toString());
        }

        try {
            if (getParameterCount() == 2) {
                String userSuppliedFormatString = values.popString();
                parseWithUserSuppliedFormat(values, userSuppliedFormatString, values.popString());
            } else {
                parseWithKnownFormats(values, values.popWhatever());
            }

        } catch (ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    abstract void parseWithUserSuppliedFormat(ValueStack values,
                                              String userSuppliedFormatString,
                                              String inputObject);

    private void parseWithKnownFormats(ValueStack values,
                                       Object inputObject) throws Exception {
        if (inputObject instanceof String) {
            values.push(CalendarFactory.asZoned((String) inputObject, adjustments()));
        } else if (inputObject instanceof TokVariable) {
            throw new Exception("unresolved variable: " + ((TokVariable) inputObject).getName());
        } else if (inputObject instanceof Long) {
            values.push(CalendarFactory.asZoned((Long) inputObject, adjustments()));
        } else if (inputObject instanceof Double) {
            values.push(CalendarFactory.asZoned(((Double) inputObject).longValue(), adjustments()));
        } else {
            values.push(CalendarFactory.asZoned((ZonedDateTime) inputObject, adjustments()));
        }
    }

    abstract String adjustments();
}
