package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncToInt extends AbstractToNumber {
    public FuncToInt(EquImpl equ) {
        super(equ);
    }

    public FuncToInt(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    protected Object popSomething(ValueStack values) throws ParseException {
        return values.popInteger();
    }

    @Override
    public String toString() {
        return "function(toInt)";
    }
}
