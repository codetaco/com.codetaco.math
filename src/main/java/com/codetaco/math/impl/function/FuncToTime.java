package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class FuncToTime extends AbstractToTemporal {

    public FuncToTime(EquImpl equ) {
        super(equ);
    }

    public FuncToTime(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    void parseWithUserSuppliedFormat(ValueStack values,
                                     String userSuppliedFormatString,
                                     String inputObject) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(userSuppliedFormatString);
        LocalTime localTime = LocalTime.parse(inputObject, dtf);
        values.push(ZonedDateTime.of(LocalDate.MIN, localTime, ZoneOffset.UTC));
    }

    @Override
    String adjustments() {
        return "=1day =1month =0year";
    }

    @Override
    public String toString() {
        return "function(toTime)";
    }
}
