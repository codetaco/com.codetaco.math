package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncStringRTrim extends Function {
    public FuncStringRTrim(EquImpl equ) {
        super(equ);
    }

    public FuncStringRTrim(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final String target = values.popString();

            int i;
            for (i = target.length() - 1; i >= 0 && Character.isWhitespace(target.charAt(i)); i--) {
                //
            }

            values.push(target.substring(0, i + 1));
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(rtrim)";
    }
}
