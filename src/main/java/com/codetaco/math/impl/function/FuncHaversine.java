package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

public class FuncHaversine extends Function {
    public FuncHaversine(EquImpl equ) {
        super(equ);
    }

    public FuncHaversine(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 4) {
            throw new Exception("missing operands for " + toString());
        }

        double R = 6372.8;

        double lon2 = values.popDouble();
        double lat2 = values.popDouble();
        double lon1 = values.popDouble();
        double lat1 = values.popDouble();

        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));

        values.push(R * c);
    }

    @Override
    public String toString() {
        return "function(haversine)";
    }
}
