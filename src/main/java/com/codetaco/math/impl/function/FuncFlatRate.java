package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.support.EquationSupport;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncFlatRate extends Function {
    public FuncFlatRate(EquImpl equ) {
        super(equ);
    }

    public FuncFlatRate(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }

        try {
            String tableName = values.popString();
            String[] keys = new String[5];
            for (int a = 0; a < getParameterCount() - 1; a++) {
                keys[a] = values.popString();
            }

            EquationSupport model = getEqu().getSupport();
            double rate = 0D;

            rate = model.resolveRate(tableName, getEqu().getBaseDate(), keys[0], keys[1], keys[2], keys[3], keys[4]);
            values.push(new Double(rate));
        } catch (ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(flatrate)";
    }
}
