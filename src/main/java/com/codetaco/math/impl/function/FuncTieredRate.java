package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.support.EquationSupport;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;
import java.util.Enumeration;
import java.util.Hashtable;

public class FuncTieredRate extends Function {
    public FuncTieredRate(EquImpl equ) {
        super(equ);
    }

    public FuncTieredRate(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < 2) {
            throw new Exception("missing operands for " + toString());
        }

        try {
            String tableName = values.popString();
            Double baseAmount = values.popDouble();

            if (baseAmount.doubleValue() == 0) {
                values.push(new Double(0D));
                return;
            }

            EquationSupport model = getEqu().getSupport();

            Hashtable<Double, Double> rateTable = model.resolveRate(tableName, getEqu().getBaseDate(), baseAmount
                                                                                                               .doubleValue());
            double blendedRate = 0;
            double previousLimit = 0;
            double previousRate = 0;

            /*
             * only the last key in the table is used for tiered rates
             */
            for (Enumeration<Double> limits = rateTable.keys(); limits.hasMoreElements(); ) {
                Double limit = limits.nextElement();
                Double rate = rateTable.get(limit);

                if (previousRate > 0) {
                    blendedRate += (previousRate * ((limit.doubleValue() - previousLimit) / baseAmount.doubleValue()));
                }

                previousRate = rate.doubleValue();
                previousLimit = limit.doubleValue();
            }

            if (previousRate > 0) {
                blendedRate += (previousRate * ((baseAmount.doubleValue() - previousLimit) / baseAmount.doubleValue()));
            }

            values.push(new Double(blendedRate));
        } catch (ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(tieredrate)";
    }
}
