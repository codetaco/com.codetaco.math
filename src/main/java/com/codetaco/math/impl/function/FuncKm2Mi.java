package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

public class FuncKm2Mi extends Function {
    public FuncKm2Mi(EquImpl equ) {
        super(equ);
    }

    public FuncKm2Mi(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < 1) {
            throw new Exception("missing operand for " + toString());
        }

        Double kilometersPerMile = 1.609344;
        Double kilometers = values.popDouble();

        values.push(kilometers / kilometersPerMile);
    }

    @Override
    public String toString() {
        return "function(km2mi)";
    }
}
