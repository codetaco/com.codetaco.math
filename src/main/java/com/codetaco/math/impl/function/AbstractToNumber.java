package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public abstract class AbstractToNumber extends Function {
    public AbstractToNumber(EquImpl equ) {
        super(equ);
    }

    public AbstractToNumber(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    protected abstract Object popSomething(ValueStack values) throws ParseException;

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            values.push(popSomething(values));
        } catch (final NumberFormatException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }
}
