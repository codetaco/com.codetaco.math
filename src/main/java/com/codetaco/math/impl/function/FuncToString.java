package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncToString extends Function {

    public FuncToString(EquImpl equ) {
        super(equ);
    }

    public FuncToString(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            String format = null;
            if (getParameterCount() == 2) {
                format = values.popString();
            }

            Object op1 = values.popWhatever();

            if (op1 instanceof String) {
                values.push(op1);
            } else if (op1 instanceof byte[]) {
                values.push(new String((byte[]) op1));
            } else if (op1 instanceof Double) {
                double op = ((Double) op1).doubleValue();
                if (format != null) {
                    values.push(String.format(format, op));
                } else {
                    values.push(Double.toString(op));
                }
            } else if (op1 instanceof Long) {
                long op = ((Long) op1).longValue();
                if (format != null) {
                    values.push(String.format(format, op));
                } else {
                    values.push(Long.toString(op));
                }
            } else {
                values.push(op1.toString());
            }
        } catch (ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(toString)";
    }
}
