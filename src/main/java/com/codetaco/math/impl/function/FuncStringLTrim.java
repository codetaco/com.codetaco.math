package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncStringLTrim extends Function {
    public FuncStringLTrim(EquImpl equ) {
        super(equ);
    }

    public FuncStringLTrim(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final String target = values.popString();

            int i;
            for (i = 0; i < target.length() && Character.isWhitespace(target.charAt(i)); i++) {
                //
            }

            values.push(target.substring(i));
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(ltrim)";
    }
}
