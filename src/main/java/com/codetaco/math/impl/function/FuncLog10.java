package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncLog10 extends Function {
    public FuncLog10(EquImpl equ) {
        super(equ);
    }

    public FuncLog10(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < 1) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            values.push(new Double(Math.log10(values.popDouble())));
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(log10)";
    }
}
