package com.codetaco.math.impl.function;

import com.codetaco.math.impl.EquImpl;
import com.codetaco.math.impl.Function;
import com.codetaco.math.impl.ValueStack;
import com.codetaco.math.impl.token.TokVariable;

import java.text.ParseException;

public class FuncMin extends Function {
    public FuncMin(EquImpl equ) {
        super(equ);
    }

    public FuncMin(EquImpl equ, TokVariable var) {
        super(equ, var);
    }

    @Override
    public void resolve(final ValueStack values) throws Exception {
        if (values.size() < getParameterCount()) {
            throw new Exception("missing operands for " + toString());
        }
        try {
            final Object[] value = values.ensureSameTypes(getParameterCount());
            if (value[0] instanceof Long) {
                long bestSoFar = (Long) value[0];
                for (int x = 1; x < value.length; x++) {
                    bestSoFar = new Long(Math.min(bestSoFar, (Long) value[x]));
                }
                values.push(new Long(bestSoFar));
            } else if (value[0] instanceof Double) {
                double bestSoFar = (Double) value[0];
                for (int x = 1; x < value.length; x++) {
                    bestSoFar = new Double(Math.min(bestSoFar, (Double) value[x]));
                }
                values.push(new Double(bestSoFar));
            }
        } catch (final ParseException e) {
            e.fillInStackTrace();
            throw new Exception(toString() + "; " + e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "function(min-" + getParameterCount() + ")";
    }
}
