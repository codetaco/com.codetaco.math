package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class ConstantsTests {
    @Test
    void pi() {
        double result = Equ.builder(Double.class).equation("PI").build().evaluate();
        assertEquals(Math.PI, result);
    }

    @Test
    void e() {
        double result = Equ.builder(Double.class).equation("E").build().evaluate();
        assertEquals(Math.E, result);
    }

    @Test
    void unknownConstant() {
        try {
            Equ.builder(Double.class).equation("W").build().evaluate();
            fail("expected exception");
        } catch (Exception e) {
            assertEquals("unresolved variable: \"W\"", e.getMessage());
        }
    }
}
