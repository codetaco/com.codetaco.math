package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class ChainingTests {
    @Test
    void testChainingToOverridePrecedence() {
        try {
            Equ.builder(Long.class)
              .equation("a:=2;+3;/4")
              .build()
              .evaluate();
            fail("Expected Exception");
        } catch (final Exception e) {
            assertEquals("missing operand", e.getMessage());
        }
    }

    @Test
    void testNothing1() {
        try {
            Equ.builder(Long.class)
              .equation("a:=2;;;")
              .build()
              .evaluate();
            fail("Expected Exception");
        } catch (final Exception e) {
            assertEquals("missing operand", e.getMessage());
        }
    }

    @Test
    void testNothing2() {
        try {
            Equ.builder(Long.class)
              .equation(";;;a:=2")
              .build()
              .evaluate();
            fail("Expected Exception");
        } catch (final Exception e) {
            assertEquals("missing operand", e.getMessage());
        }
    }

    @Test
    void testPresetVariables() {
        Long expectedResult = 6L;
        Long expectedA = 2L;
        Long expectedB = 3L;
        Long expectedC = 6L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("a:=2; b:=a+1; c:=a*b")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedA, equ.getLong("a"));
        assertEquals(expectedB, equ.getLong("b"));
        assertEquals(expectedC, equ.getLong("c"));
    }

    @Test
    void testProofOfPrecedence() {
        Double expectedResult = 2.75D;
        Long expectedA = 2L;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("(a:=2)+3/4")
                            .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedA, equ.getLong("a"));
    }

    @Test
    void testProofOfPrecedence2() {
        Double expectedResult = 2.75D;
        Double expectedA = 2.75D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("a:=2+3/4")
                            .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedA, equ.getDouble("a"));
    }

    @Test
    void testTrailingChainMarker() {
        try {
            Equ.builder(Long.class)
              .equation("a:=2;b:=a+1;c:=a*b;")
              .build()
              .evaluate();
            fail("Expected Exception");
        } catch (final Exception e) {
            assertEquals("missing operand", e.getMessage());
        }
    }

    @Test
    void testTrailingChainMarker2() {
        try {
            Equ.builder(Long.class)
              .equation("a:=2; b:=a+1; a*b;")
              .build()
              .evaluate();
            fail("Expected Exception");
        } catch (final Exception e) {
            assertEquals("missing operand", e.getMessage());
        }
    }

    @Test
    void testUnexpectedChaining() {
        try {
            Equ.builder(Long.class)
              .equation("a:=2;*3")
              .build()
              .evaluate();
            fail("Expected Exception");
        } catch (final Exception e) {
            assertEquals("missing operand", e.getMessage());
        }
    }
}
