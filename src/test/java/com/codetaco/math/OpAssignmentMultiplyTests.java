package com.codetaco.math;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OpAssignmentMultiplyTests {

    @ParameterizedTest
    @ValueSource(strings = {
      "a:=toInt(5000);b:=toInt(2);toInt(a*=b)",
      "a:=toInt(5000);b:=toLong(2);toInt(a*=b)",
      "a:=toInt(5000);b:=toString(2);toInt(a*=b)",
      "a:=toInt(5000);b:=toFloat(2);toInt(a*=b)",
      "a:=toInt(5000);b:=toDouble(2);toInt(a*=b)",

      "a:=toLong(5000);b:=toLong(2);toInt(a*=b)",
      "a:=toLong(5000);b:=toString(2);toInt(a*=b)",
      "a:=toLong(5000);b:=toInt(2);toInt(a*=b)",
      "a:=toLong(5000);b:=toFloat(2);toInt(a*=b)",
      "a:=toLong(5000);b:=toDouble(2);toInt(a*=b)",

      "a:=toFloat(5000);b:=toFloat(2);toInt(a*=b)",
      "a:=toFloat(5000);b:=toString(2);toInt(a*=b)",
      "a:=toFloat(5000);b:=toInt(2);toInt(a*=b)",
      "a:=toFloat(5000);b:=toLong(2);toInt(a*=b)",
      "a:=toFloat(5000);b:=toDouble(2);toInt(a*=b)",

      "a:=toDouble(5000);b:=toDouble(2);toInt(a*=b)",
      "a:=toDouble(5000);b:=toString(2);toInt(a*=b)",
      "a:=toDouble(5000);b:=toInt(2);toInt(a*=b)",
      "a:=toDouble(5000);b:=toLong(2);toInt(a*=b)",
      "a:=toDouble(5000);b:=toFloat(2);toInt(a*=b)",

      "a:=toString(5000);b:=toDouble(2);toInt(a*=b)",
      "a:=toString(5000);b:=toInt(2);toInt(a*=b)",
      "a:=toString(5000);b:=toLong(2);toInt(a*=b)",
      "a:=toString(5000);b:=toFloat(2);toInt(a*=b)"
    })
    void varVar(String equString) {
        int expectedResult = 10000;
        Equ<Integer> equ = Equ.builder(Integer.class)
                             .equation(equString)
                             .build();
        int result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {
      "a:=toDate(5000);b:=toDate(2);toInt(a*=b)",
      "a:=toString(5000);b:=toString(2);toInt(a*=b)"
    })
    void invalidTypes(String equString) {
        Throwable exception = assertThrows(MathException.class, () -> {
            Equ.builder(Integer.class)
              .equation(equString)
              .build()
              .evaluate();
        });
        assertEquals("invalid types", exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {
      "toInt(a*=1)"
    })
    void invalidTarget(String equString) {
        Throwable exception = assertThrows(MathException.class, () -> {
            Equ.builder(Integer.class)
              .equation(equString)
              .build()
              .evaluate();
        });
        assertEquals("invalid assignment target: var(a)", exception.getMessage());
    }
}
