package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MatchTests {
    @Test
    void stringCaseInsensitive() {
        String expected = "ABC";
        String result = Equ.builder(String.class)
                          .equation("match('ABC123XYZ', '(?i:abc)')")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringEmpty() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("empty(match('ABC123XYZ', 'NOTHING'))")
                     .build()
                     .evaluate());
    }

    @Test
    void stringLength() {
        double expected = 3D;
        double result = Equ.builder(Double.class)
                          .equation("length(match('ABC123XYZ', '[X-Z]+'))")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringMatches() {
        String expected = "ABC123XYZ";
        String result = Equ.builder(String.class)
                          .equation("match('ABC123XYZ', '.*123.*')")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringMatchesInitialGroup() {
        String expected = "ABC123XYZ";
        String result = Equ.builder(String.class)
                          .equation("match('ABC123XYZ', '.*(123).*')")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringMatchesFirstGroup() {
        String expected = "123";
        String result = Equ.builder(String.class)
                          .equation("match('ABC123XYZ', '[^0-9]*([123]+)[^0-9]*', 1)")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringMatchesPartial() {
        String expected = "123";
        String result = Equ.builder(String.class)
                          .equation("match('ABC123XYZ', '123')")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringNotEmpty() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("not(empty(match('ABC123XYZ', '123')))")
                     .build()
                     .evaluate());
    }

    @Test
    void matchesFalse() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("matches('ABC123XYZ', '123')")
                      .build()
                      .evaluate());
    }

    @Test
    void matchesTrue() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("matches('ABC123XYZ', '[A-Z]+[0-9]+[A-Z]+')")
                     .build()
                     .evaluate());
    }

    @Test
    void containsTrue() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("contains('ABC123XYZ', '123')")
                     .build()
                     .evaluate());
    }

    @Test
    void containsFalse() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("contains('ABC123XYZ', 'CHRIS')")
                      .build()
                      .evaluate());
    }

    @Test
    void containsAllTrue() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("contains('ABC123XYZ', '[A-Z]+[0-9]+[A-Z]+')")
                     .build()
                     .evaluate());
    }

    @Test
    void varToStringCaseMatters() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("target:='ABC123XYZ';" +
                                 "pattern:='(?i:c)';" +
                                 "contains(target, pattern)")
                     .build()
                     .evaluate());
    }

    @Test
    void varToStringCaseMattersVars() {
        assertTrue(Equ.builder(Boolean.class)
                     .variable("target", "ABC123XYZ")
                     .variable("pattern", "(?i:c)")
                     .equation("contains(target, pattern)")
                     .build()
                     .evaluate());
    }

    @Test
    void varToStringNoCase() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("a:='ABC123XYZ';b:='c';contains(a, b)")
                      .build()
                      .evaluate());
    }
}
