package com.codetaco.math;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OpMultiplyTests {

    @ParameterizedTest
    @CsvSource({

      "a:=toDate('11-29-2018');b:=1;toInt(a*b),1556340736",
      "a:=toDate('11-29-2018');b:=1;toInt(b*a),1556340736",

      "a:=toInt(5000);b:=toInt(2);toInt(a*b),10000",
      "a:=toInt(5000);b:=toLong(2);toInt(a*b),10000",
      "a:=toInt(5000);b:=toString(2);toInt(a*b),10000",
      "a:=toInt(5000);b:=toFloat(2);toInt(a*b),10000",
      "a:=toInt(5000);b:=toDouble(2);toInt(a*b),10000",

      "a:=toLong(5000);b:=toLong(2);toInt(a*b),10000",
      "a:=toLong(5000);b:=toString(2);toInt(a*b),10000",
      "a:=toLong(5000);b:=toInt(2);toInt(a*b),10000",
      "a:=toLong(5000);b:=toFloat(2);toInt(a*b),10000",
      "a:=toLong(5000);b:=toDouble(2);toInt(a*b),10000",

      "a:=toFloat(5000);b:=toFloat(2);toInt(a*b),10000",
      "a:=toFloat(5000);b:=toString(2);toInt(a*b),10000",
      "a:=toFloat(5000);b:=toInt(2);toInt(a*b),10000",
      "a:=toFloat(5000);b:=toLong(2);toInt(a*b),10000",
      "a:=toFloat(5000);b:=toDouble(2);toInt(a*b),10000",

      "a:=toDouble(5000);b:=toDouble(2);toInt(a*b),10000",
      "a:=toDouble(5000);b:=toString(2);toInt(a*b),10000",
      "a:=toDouble(5000);b:=toInt(2);toInt(a*b),10000",
      "a:=toDouble(5000);b:=toLong(2);toInt(a*b),10000",
      "a:=toDouble(5000);b:=toFloat(2);toInt(a*b),10000",

      "a:=toString(5000);b:=toDouble(2);toInt(a*b),10000",
      "a:=toString(5000);b:=toInt(2);toInt(a*b),10000",
      "a:=toString(5000);b:=toLong(2);toInt(a*b),10000",
      "a:=toString(5000);b:=toFloat(2);toInt(a*b),10000"
    })
    void varVar(String equString, int expectedResult) {
        Equ<Integer> equ = Equ.builder(Integer.class)
                             .equation(equString)
                             .build();
        int result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {
      "a:=toString(5000);b:=toString(2);toInt(a*b)",
    })
    void invalidTypes(String equString) {
        Throwable exception = assertThrows(MathException.class, () -> {
            Equ.builder(Integer.class)
              .equation(equString)
              .build()
              .evaluate();
        });
        assertEquals("invalid types", exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {
      "toInt(a * 1)",
      "toInt(1*a)"
    })
    void unassigned(String equString) {
        Throwable exception = assertThrows(MathException.class, () -> {
            Equ.builder(Integer.class)
              .equation(equString)
              .build()
              .evaluate();
        });
        assertEquals("op(multiply); \"a\" is unassigned", exception.getMessage());
    }
}
