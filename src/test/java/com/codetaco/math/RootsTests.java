package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RootsTests {

    @Test
    void sqrt() {

        double expected = 3;
        double result = Equ.builder(Double.class)
                          .equation("sqrt(9)")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void cbrt() {

        double expected = 3;
        double result = Equ.builder(Double.class)
                          .equation("cbrt(27)")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void cbrt2() {

        double expected = 9;
        double result = Equ.builder(Double.class)
                          .equation("cbrt(9*9*9)")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void sqrt2() {

        double expected = 27;
        double result = Equ.builder(Double.class)
                          .equation("sqrt(9*9*9)")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void srootOFcroot() {

        double expected = 3;
        double result = Equ.builder(Double.class)
                          .equation("sqrt(cbrt(9*9*9))")
                          .evaluate();
        assertEquals(expected, result, 5D);
    }

    @Test
    void crootOFsroot() {

        double expected = 3;
        double result = Equ.builder(Double.class)
                          .equation("cbrt(sqrt(9*9*9))")
                          .evaluate();
        assertEquals(expected, result, 5D);
    }

    @Test
    void root2() {

        double expected = 4;
        double result = Equ.builder(Double.class)
                          .equation("root(16, 2))")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void root4() {

        double expected = 9;
        double result = Equ.builder(Double.class)
                          .equation("root(9*9*9*9, 4))")
                          .evaluate();
        assertEquals(expected, result);
    }
}
