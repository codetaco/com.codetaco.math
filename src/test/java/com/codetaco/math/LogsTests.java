package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LogsTests {

    private double delta3 = 3D;

    @Test
    void logOfe() {

        double expected = 1D;
        double result = Equ.builder(Double.class)
                          .equation("log(e)")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void log() {

        double expected = 0D;
        double result = Equ.builder(Double.class)
                          .equation("log(1)")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void logESquared() {

        double expected = 2D;
        double result = Equ.builder(Double.class)
                          .equation("log(e^2)")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void logEpow() {

        double expected = 10D;
        double result = Equ.builder(Double.class)
                          .equation("log(e^10)")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void log10() {

        double expected = 2D;
        double result = Equ.builder(Double.class)
                          .equation("log10(100)")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void log100() {

        double expected = Double.NEGATIVE_INFINITY;
        double result = Equ.builder(Double.class)
                          .equation("log10(0)")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void log101() {

        double expected = 0D;
        double result = Equ.builder(Double.class)
                          .equation("log10(1)")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void log1010() {

        double expected = 1D;
        double result = Equ.builder(Double.class)
                          .equation("log10(10)")
                          .build()
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void log10round() {

        double expected = 3D;
        double result = Equ.builder(Double.class)
                          .equation("round(log10(1050), 0)")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta3);
    }

    @Test
    void log10trunc() {

        double expected = 2D;
        double result = Equ.builder(Double.class)
                          .equation("trunc(log10(950), 0)")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta3);
    }

    @Test
    void log10round3() {

        double expected = 3D;
        double result = Equ.builder(Double.class)
                          .equation("round(log10(950), 0)")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta3);
    }

    @Test
    void log10round2() {

        double expected = 3.02D;
        double result = Equ.builder(Double.class)
                          .equation("round(log10(1050), 2)")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta3);
    }

}
