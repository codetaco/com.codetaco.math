package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class HaversineTests {
    @Test
    void error() {
        try {
            Equ.builder(Double.class).equation("a:=haversine(,,,)").build().evaluate();
            fail("Expected Exception");
        } catch (Exception e) {
            assertEquals("missing operands for function(haversine)", e.getMessage());
        }
    }

    @Test
    void testFromRosettacode() {
        double result = Equ.builder(Double.class)
                          .equation("haversine(36.12, -86.67, 33.94, -118.40)")
                          .build()
                          .evaluate();
        assertEquals(2887.2599506071106, result, .0000000000001D);
    }

    @Test
    void inMiles() {
        double result = Equ.builder(Double.class)
                          .equation("km2mi(haversine(36.12, -86.67, 33.94, -118.40))")
                          .build()
                          .evaluate();
        assertEquals(1794.060157807846,
                     result,
                     .0000000000001D);
    }

    @Test
    void allZeroes() {
        double result = Equ.builder(Double.class)
                          .equation("haversine(0,0,0,0)")
                          .build()
                          .evaluate();
        assertEquals(0,
                     result,
                     .0000000000001D);
    }

    @Test
    void samePointTwice() {
        double result = Equ.builder(Double.class)
                          .equation("haversine(36.12, -86.67, 36.12, -86.67)")
                          .build()
                          .evaluate();
        assertEquals(0,
                     result,
                     .0000000000001D);
    }
}
