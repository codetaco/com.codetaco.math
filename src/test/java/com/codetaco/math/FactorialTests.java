package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class FactorialTests {
    @Test
    void binomialCoefficient() {
        double result = Equ.builder(Double.class)
                          .equation("(n^k)/(k!)")
                          .variable("n", 5D)
                          .variable("k", 6D)
                          .build()
                          .evaluate();
        assertEquals(21.7, result, 1D);
    }

    @Test
    void fac1() {
        long result = Equ.builder(Long.class)
                        .equation("3!")
                        .build()
                        .evaluate();
        assertEquals(6, result);
    }

    @Test
    void fraction() {
        long result = Equ.builder(Long.class)
                        .equation("5.4!")
                        .build()
                        .evaluate();
        assertEquals(120, result);
    }

    @Test
    void negative() {
        try {
            Equ.builder(Double.class)
              .equation("-5!")
              .build()
              .evaluate();
            fail("exception expected but not thrown");
        } catch (final Exception e) {
            assertEquals("op(factorial); negative numbers not allowed", e.getMessage());
        }
    }

    @Test
    void precedence1() {
        long result = Equ.builder(Long.class)
                        .equation("3*2!")
                        .build()
                        .evaluate();
        assertEquals(6, result);
    }

    @Test
    void precedence2() {
        long result = Equ.builder(Long.class)
                        .equation("3!*-2")
                        .build()
                        .evaluate();
        assertEquals(-12, result);
    }

    @Test
    void preliminaryResult() {
        long result = Equ.builder(Long.class)
                        .equation("(3*2)!")
                        .build()
                        .evaluate();
        assertEquals(720, result);
    }

    @Test
    void tooLarge() {
        try {
            Equ.builder(Double.class)
              .equation("21!")
              .build()
              .evaluate();
            fail("exception expected but not thrown");
        } catch (final Exception e) {
            assertEquals("op(factorial); numeric overflow", e.getMessage());
        }
    }

    @Test
    void veryLarge() {
        long result = Equ.builder(Long.class)
                        .equation("20!")
                        .build()
                        .evaluate();
        assertEquals(2432902008176640000L, result);
    }

    @Test
    void zero() {
        long result = Equ.builder(Long.class)
                        .equation("0!")
                        .build()
                        .evaluate();
        assertEquals(1, result);
    }
}
