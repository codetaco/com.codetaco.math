package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AndOrTests {
    @Test
    void conditionalAND1() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("true && true")
                     .build()
                     .evaluate());
    }

    @Test
    void conditionalAND2() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("true && false")
                      .build()
                      .evaluate());
    }

    @Test
    void conditionalAND3() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("true && not(false)")
                     .build()
                     .evaluate());
    }

    @Test
    void conditionalAND4() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("not(true && false)")
                     .build()
                     .evaluate());
    }

    @Test
    void conditionalAND5() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("false && false")
                      .build()
                      .evaluate());
    }

    @Test
    void conditionalOR1() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("true || true")
                     .build()
                     .evaluate());
    }

    @Test
    void conditionalOR2() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("true || false")
                     .build()
                     .evaluate());
    }

    @Test
    void conditionalOR3() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("false || not(false)")
                     .build()
                     .evaluate());
    }

    @Test
    void conditionalOR4() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("not(true || false)")
                      .build()
                      .evaluate());
    }

    @Test
    void conditionalOR5() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("false || false")
                      .build()
                      .evaluate());
    }

    @Test
    void conditionalPrecedence() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("not true && false")
                      .build()
                      .evaluate());
    }

    @Test
    void greaterAndLess() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("a:=43;a>=40 && a<50")
                     .build()
                     .evaluate());
    }

    @Test
    void nand1() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("true !& false")
                     .build()
                     .evaluate());
    }

    @Test
    void nand2() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("true !& true")
                      .evaluate());
    }

    @Test
    void nand3() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("false !& true")
                     .build()
                     .evaluate());
    }

    @Test
    void nand4() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("false !& false")
                     .build()
                     .evaluate());
    }

    @Test
    void nor1() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("false !| false")
                     .build()
                     .evaluate());
    }

    @Test
    void nor2() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("false !| true")
                      .build()
                      .evaluate());
    }

    @Test
    void nor3() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("true !| false")
                      .build()
                      .evaluate());
    }

    @Test
    void nor4() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("true !| true")
                      .build()
                      .evaluate());
    }

    @Test
    void xnor1() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("false !~| false")
                     .build()
                     .evaluate());
    }

    @Test
    void xnor2() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("false !~| true")
                      .build()
                      .evaluate());
    }

    @Test
    void xnor3() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("true !~| false")
                      .build()
                      .evaluate());
    }

    @Test
    void xnor4() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("true !~| true")
                     .build()
                     .evaluate());
    }

    @Test
    void xor1() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("false ~| false")
                      .build()
                      .evaluate());
    }

    @Test
    void xor2() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("false ~| true")
                     .build()
                     .evaluate());
    }

    @Test
    void xor3() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("true ~| false")
                     .build()
                     .evaluate());
    }

    @Test
    void xor4() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("true ~| true")
                      .build()
                      .evaluate());
    }

}
