package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class ConversionsTests {
    @Test
    void km2miError() {
        try {
            Equ.builder(Double.class)
              .equation("km2mi()")
              .evaluate();
            fail("Expected Exception");
        } catch (Exception e) {
            assertEquals("missing operand for function(km2mi)", e.getMessage());
        }
    }

    @Test
    void mi2kmError() {
        try {
            Equ.builder(Double.class)
              .equation("mi2km()")
              .evaluate();
            fail("Expected Exception");
        } catch (Exception e) {
            assertEquals("missing operand for function(mi2km)", e.getMessage());
        }
    }

    @Test
    void km2mi() {
        double expectedResult = 0.621371192237334D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("km2mi(1)")
                            .build();
        assertEquals(expectedResult, equ.evaluate(), .0000000000001D);
    }

    @Test
    void mi2km() {
        double expectedResult = 1.609344D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("mi2km(1)")
                            .build();
        assertEquals(expectedResult, equ.evaluate(), .0000000000001D);
    }

    @Test
    void km2mi0() {

        double expectedResult = 0D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("km2mi(0)")
                            .build();
        assertEquals(expectedResult, equ.evaluate(), .0000000000001D);
    }

    @Test
    void mi2km0() {
        double expectedResult = 0D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("mi2km(0)")
                            .build();
        assertEquals(expectedResult, equ.evaluate(), .0000000000001D);
    }

    @Test
    void convertIntToInt() {
        int expectedResult = 5000;
        Equ<Integer> equ = Equ.builder(Integer.class)
                             .equation("toInt(toInt(5000))")
                             .build();
        int result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @Test
    void convertLongToInt() {
        int expectedResult = 5000;
        Equ<Integer> equ = Equ.builder(Integer.class)
                             .equation("toInt(5 * 1000)")
                             .build();
        int result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @Test
    void convertStringToInt() {
        int expectedResult = 5000;
        Equ<Integer> equ = Equ.builder(Integer.class)
                             .equation("toInt('5000')")
                             .build();
        int result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @Test
    void convertDoubleToInt() {
        int expectedResult = 5000;
        Equ<Integer> equ = Equ.builder(Integer.class)
                             .equation("toInt(5 * 1000.123)")
                             .build();
        int result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @Test
    void converIntToLong() {
        long expectedResult = 5000;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("toLong(toInt(5000))")
                          .build();
        long result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @Test
    void convertLongToLong() {
        long expectedResult = 5000;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("toLong(5 * 1000)")
                          .build();
        long result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @Test
    void convertStringToLong() {
        long expectedResult = 5000;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("toLong('5000')")
                          .build();
        long result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @Test
    void convertDoubleToLong() {
        long expectedResult = 5000;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("toLong(5 * 1000.123)")
                          .build();
        long result = equ.evaluate();
        assertEquals(expectedResult, result, .0000000000001D);
    }

    @Test
    void converIntToFloat() {
        float expectedResult = 5000;
        Equ<Float> equ = Equ.builder(Float.class)
                           .equation("toFloat(toInt(5000))")
                           .build();
        float result = equ.evaluate();
        assertEquals(expectedResult, result, .0000000000001D);
    }

    @Test
    void convertLongToFloat() {
        float expectedResult = 5000;
        Equ<Float> equ = Equ.builder(Float.class)
                           .equation("toFloat(5 * 1000)")
                           .build();
        float result = equ.evaluate();
        assertEquals(expectedResult, result, .0000000000001D);
    }

    @Test
    void convertStringToFloat() {
        long expectedResult = 5000;
        Equ<Float> equ = Equ.builder(Float.class)
                           .equation("toFloat('5000')")
                           .build();
        float result = equ.evaluate();
        assertEquals(expectedResult, result, .0000000000001D);
    }

    @Test
    void convertDoubleToFloat() {
        float expectedResult = 5000.615F;
        Equ<Float> equ = Equ.builder(Float.class)
                           .equation("toFloat(5 * 1000.123)")
                           .build();
        float result = equ.evaluate();
        assertEquals(expectedResult, result, .0000000000001D);
    }

    @Test
    void converIntToDouble() {
        double expectedResult = 5000;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("toDouble(toInt(5000))")
                            .build();
        double result = equ.evaluate();
        assertEquals(expectedResult, result, .0000000000001D);
    }

    @Test
    void convertLongToDouble() {
        double expectedResult = 5000;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("toDouble(5 * 1000)")
                            .build();
        double result = equ.evaluate();
        assertEquals(expectedResult, result, .0000000000001D);
    }

    @Test
    void convertStringToDouble() {
        double expectedResult = 5000;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("toDouble('5000')")
                            .build();
        double result = equ.evaluate();
        assertEquals(expectedResult, result, .0000000000001D);
    }

    @Test
    void convertDoubleToDouble() {
        double expectedResult = 5000.615D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("toDouble(5 * 1000.123)")
                            .build();
        double result = equ.evaluate();
        assertEquals(expectedResult, result, .0000000000001D);
    }

    @Test
    void converIntToString() {
        String expectedResult = "5000";
        Equ<String> equ = Equ.builder(String.class)
                            .equation("toString(toInt(5000))")
                            .build();
        String result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @Test
    void convertLongToString() {
        String expectedResult = "5000";
        Equ<String> equ = Equ.builder(String.class)
                            .equation("toString(5 * 1000)")
                            .build();
        String result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @Test
    void convertStringToString() {
        String expectedResult = "5000";
        Equ<String> equ = Equ.builder(String.class)
                            .equation("toString('5000')")
                            .build();
        String result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @Test
    void convertDoubleToString() {
        String expectedResult = "5000.615";
        Equ<String> equ = Equ.builder(String.class)
                            .equation("toString(5 * 1000.123)")
                            .build();
        String result = equ.evaluate();
        assertEquals(expectedResult, result);
    }
}
