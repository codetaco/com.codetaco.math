package com.codetaco.math;

import com.codetaco.math.impl.function.FuncMin;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RegisterFunctionTests {

    @Test
    void myFunction() {
        long expected = 3;
        long result = Equ.builder(Long.class)
                        .function("myFunction", FuncMin.class)
                        .equation("myFunction(5,max(2,3))")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }
}
