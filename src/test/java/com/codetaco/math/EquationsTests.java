package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EquationsTests {

    private static final String X = "x";

    @Test
    void plusPlus() {
        long result = Equ.builder(Long.class).equation("2++").build().evaluate();
        assertEquals(3, result);
    }

    @Test
    void minusMinus() {
        long result = Equ.builder(Long.class).equation("5--").build().evaluate();
        assertEquals(4, result);
    }

    @Test
    void booleanEqual() {
        boolean result = Equ.builder(Boolean.class).equation("1 = 2").build().evaluate();
        assertFalse(result);
    }

    @Test
    void booleanGreater() {
        boolean result = Equ.builder(Boolean.class).equation("1 > 2").build().evaluate();
        assertFalse(result);
    }

    @Test
    void booleanLess() {
        boolean result = Equ.builder(Boolean.class).equation("1 < 2").build().evaluate();
        assertTrue(result);
    }

    @Test
    void booleanNotEqual() {
        boolean result = Equ.builder(Boolean.class).equation("1 != 2").build().evaluate();
        assertTrue(result);
    }

    @Test
    void booleanNotGreater() {
        boolean result = Equ.builder(Boolean.class).equation("1 <= 2").build().evaluate();
        assertTrue(result);
    }

    @Test
    void booleanNotLess() {
        boolean result = Equ.builder(Boolean.class).equation("1 >= 2").build().evaluate();
        assertFalse(result);
    }

    @Test
    void m10a() {
        long result = Equ.builder(Long.class).equation("if (1=1,1,2.5)").build().evaluate();
        assertEquals(1, result);
    }

    @Test
    void m10b() {
        double result = Equ.builder(Double.class).equation("if (1=3,1,2.5)").build().evaluate();
        assertEquals(2.5D, result);
    }

    @Test
    void m10c() {
        double result = Equ.builder(Double.class).equation("5 * if (1=3,1,2.0)").build().evaluate();
        assertEquals(10D, result);
    }

    @Test
    void m10d() {
        long result = Equ.builder(Long.class).equation("if (1=3,1,if (10 - 9 = 11 - 10,4,5))").build().evaluate();
        assertEquals(4, result);
    }

    @Test
    void m1a() {
        double result = Equ.builder(Double.class)
                          .equation("2x")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(10D, result);
    }

    @Test
    void m1b() {
        double result = Equ.builder(Double.class)
                          .equation("2tan(x)")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(0.174977327051848, result);
    }

    @Test
    void m2() {
        double result = Equ.builder(Double.class)
                          .equation("2(3+x)")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(16D, result);
    }

    @Test
    void m3a() {
        double result = Equ.builder(Double.class)
                          .equation("(2-x)x")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(-15D, result);
    }

    @Test
    void m3b() {
        double result = Equ.builder(Double.class)
                          .equation("(2-x)sin(x)")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(-0.2614672282429745, result);
    }

    @Test
    void m4a() {
        double result = Equ.builder(Double.class)
                          .equation("(2-x)(x+1)")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(-18D, result);
    }

    @Test
    void m4b() {
        double result = Equ.builder(Double.class)
                          .equation("sin(x)(2-x)")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(-0.2614672282429745, result);
    }

    @Test
    void m5a() {
        double result = Equ.builder(Double.class)
                          .equation("x(x+1)")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(30D, result);
    }

    @Test
    void m5b() {
        double result = Equ.builder(Double.class)
                          .equation("x(1-sin(x))")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(4.564221286261709, result);
    }

    @Test
    void m6a() {
        double result = Equ.builder(Double.class)
                          .equation("3 / (4 * x)11 / 2")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(0.825, result);
    }

    @Test
    void m6b() {
        double result = Equ.builder(Double.class)
                          .equation("3 / 4 * x * 11 / 2")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(20.625, result);
    }

    @Test
    void m6c() {
        double result = Equ.builder(Double.class)
                          .equation("4 - 7 + 11 / 3 * x")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(15.333333333333332, result);
    }

    @Test
    void m7a() {
        long result = Equ.builder(Long.class).equation("max(1,2,3)").build().evaluate();
        assertEquals(3, result);
    }

    @Test
    void m7b() {
        double result = Equ.builder(Double.class)
                          .equation("max(x,8,2,1)")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(8, result);
    }

    @Test
    void m7c() {
        double result = Equ.builder(Double.class)
                          .equation("max(x,max(x+2,6),1)")
                          .variable(X, 5D)
                          .build()
                          .evaluate();
        assertEquals(7, result);
    }

    @Test
    void m8a() {
        double result = Equ.builder(Double.class)
                          .equation("FacAmt*(100-Age)/Prem*12358+FacAmt ")
                          .variable("FacAmt", 1500000D)
                          .variable("Age", 55D)
                          .variable("Prem", 200000D)
                          .build()
                          .evaluate();
        assertEquals(5670825.00, result);
    }

    @Test
    void m8b() {
        double result = Equ.builder(Double.class)
                          .equation("FacAmt*(100-Age)/Prem*(12358+FacAmt) ")
                          .variable("FacAmt", 1500000D)
                          .variable("Age", 55D)
                          .variable("Prem", 200000D)
                          .build()
                          .evaluate();
        assertEquals(510420825.00, result);
    }

    @Test
    void m8c() {
        double result = Equ.builder(Double.class)
                          .equation("round((FacAmt*100-Age)/Prem*12358+FacAmt,2)")
                          .variable("FacAmt", 1500000D)
                          .variable("Age", 55D)
                          .variable("Prem", 200000D)
                          .build()
                          .evaluate();
        assertEquals(10768496.60, result);
    }

    @Test
    void m9a() {
        String result = Equ.builder(String.class)
                          .equation("alpha(round(FacAmt,0))")
                          .variable("FacAmt", 1500000.99D)
                          .build()
                          .evaluate();
        assertEquals("1500001", result);
    }

    @Test
    void m9a2() {
        String result = Equ.builder(String.class)
                          .equation("alpha(FacAmt)")
                          .variable("FacAmt", 1500000.99D)
                          .build()
                          .evaluate();
        assertEquals("1500000", result);
    }

    @Test
    void m9b() {
        double result = Equ.builder(Double.class)
                          .equation("rate('tablename', 'a', 'b')")
                          .build()
                          .evaluate();
        assertEquals(1D, result);
    }

    @Test
    void m9c() {
        double result = Equ.builder(Double.class)
                          .equation("rate('tablename', 'a', alpha(FacAmt))")
                          .variable("FacAmt", 1500000D)
                          .build()
                          .evaluate();
        assertEquals(1D, result);
    }

    @Test
    void maxAndMinFunctions() {
        long result = Equ.builder(Long.class).equation("max(1,0) + min(2,0)").build().evaluate();
        assertEquals(1, result);
    }

    @Test
    void min() {
        double result = Equ.builder(Double.class)
                          .equation("min(1,2.0,3)")
                          .build()
                          .evaluate();
        assertEquals(1, result);
    }

    @Test
    void round() {
        double result = Equ.builder(Double.class)
                          .equation("round(10.495,1)")
                          .build()
                          .evaluate();
        assertEquals(10.5D, result);
    }

    @Test
    void twoMaxDoubleFunctions() {
        double result = Equ.builder(Double.class)
                          .equation("max(1,2.25) + max(2,0)")
                          .build()
                          .evaluate();
        assertEquals(4.25, result);
    }

    @Test
    void twoMaxFunctions() {
        long result = Equ.builder(Long.class).equation("max(1,0) + max(2,0)").build().evaluate();
        assertEquals(3, result);
    }
}
