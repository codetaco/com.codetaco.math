package com.codetaco.math;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OpMinusTests {

    @ParameterizedTest
    @CsvSource({

      "a:=toDate('11-29-2018');b:=1;toInt(a-b),1556340735",
      "a:=toDate('11-29-2018');b:=1;toInt(b-a),-1556340735",

      "a:=toInt(5000);b:=toInt(1);toInt(a-b),4999",
      "a:=toInt(5000);b:=toLong(1);toInt(a-b),4999",
      "a:=toInt(5000);b:=toString(1);toInt(a-b),4999",
      "a:=toInt(5000);b:=toFloat(1);toInt(a-b),4999",
      "a:=toInt(5000);b:=toDouble(1);toInt(a-b),4999",

      "a:=toLong(5000);b:=toLong(1);toInt(a-b),4999",
      "a:=toLong(5000);b:=toString(1);toInt(a-b),4999",
      "a:=toLong(5000);b:=toInt(1);toInt(a-b),4999",
      "a:=toLong(5000);b:=toFloat(1);toInt(a-b),4999",
      "a:=toLong(5000);b:=toDouble(1);toInt(a-b),4999",

      "a:=toFloat(5000);b:=toFloat(1);toInt(a-b),4999",
      "a:=toFloat(5000);b:=toString(1);toInt(a-b),4999",
      "a:=toFloat(5000);b:=toInt(1);toInt(a-b),4999",
      "a:=toFloat(5000);b:=toLong(1);toInt(a-b),4999",
      "a:=toFloat(5000);b:=toDouble(1);toInt(a-b),4999",

      "a:=toDouble(5000);b:=toDouble(1);toInt(a-b),4999",
      "a:=toDouble(5000);b:=toString(1);toInt(a-b),4999",
      "a:=toDouble(5000);b:=toInt(1);toInt(a-b),4999",
      "a:=toDouble(5000);b:=toLong(1);toInt(a-b),4999",
      "a:=toDouble(5000);b:=toFloat(1);toInt(a-b),4999",

      "a:=toString(5000);b:=toDouble(1);toInt(a-b),4999",
      "a:=toString(5000);b:=toInt(1);toInt(a-b),4999",
      "a:=toString(5000);b:=toLong(1);toInt(a-b),4999",
      "a:=toString(5000);b:=toFloat(1);toInt(a-b),4999"
    })
    void varVar(String equString, int expectedResult) {
        Equ<Integer> equ = Equ.builder(Integer.class)
                             .equation(equString)
                             .build();
        int result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {
      "a:=toString(5000);b:=toString(1);toInt(a-b)",
    })
    void invalidTypes(String equString) {
        Throwable exception = assertThrows(MathException.class, () -> {
            Equ.builder(Integer.class)
              .equation(equString)
              .build()
              .evaluate();
        });
        assertEquals("invalid types", exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {
      "toInt(a - 1)",
      "toInt(1-a)"
    })
    void unassigned(String equString) {
        Throwable exception = assertThrows(MathException.class, () -> {
            Equ.builder(Integer.class)
              .equation(equString)
              .build()
              .evaluate();
        });
        assertEquals("op(minus); \"a\" is unassigned", exception.getMessage());
    }
}
