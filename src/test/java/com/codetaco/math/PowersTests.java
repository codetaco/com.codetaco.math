package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PowersTests {
    @Test
    void power1() {
        long expected = 32;
        long result = Equ.builder(Long.class)
                        .equation("2^5")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void power2() {
        long expected = 96;
        long result = Equ.builder(Long.class)
                        .equation("2^5*3")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void power3() {
        long expected = 96;
        long result = Equ.builder(Long.class)
                        .equation("3*2^5")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void power4() {
        long expected = 7776;
        long result = Equ.builder(Long.class)
                        .equation("(3*2)^5")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void power5() {
        long expected = 32768;
        long result = Equ.builder(Long.class)
                        .equation("2^(5*3)")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void powerFloat() {
        double delta = 0.0001;
        double expected = 49667.0004D;
        double result = Equ.builder(Double.class)
                          .equation("2^(5.2*3)")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }
}
