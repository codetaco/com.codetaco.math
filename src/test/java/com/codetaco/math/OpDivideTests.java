package com.codetaco.math;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OpDivideTests {

    @ParameterizedTest
    @CsvSource({

      "a:=toDate('11-29-2018');b:=2000;toInt(a/b),771724800",
      "a:=toDate('11-29-2018');b:=2;toInt(b/a),0",

      "a:=toInt(5000);b:=toInt(2);toInt(a/b),2500",
      "a:=toInt(5000);b:=toLong(2);toInt(a/b),2500",
      "a:=toInt(5000);b:=toString(2);toInt(a/b),2500",
      "a:=toInt(5000);b:=toFloat(2);toInt(a/b),2500",
      "a:=toInt(5000);b:=toDouble(2);toInt(a/b),2500",

      "a:=toLong(5000);b:=toLong(2);toInt(a/b),2500",
      "a:=toLong(5000);b:=toString(2);toInt(a/b),2500",
      "a:=toLong(5000);b:=toInt(2);toInt(a/b),2500",
      "a:=toLong(5000);b:=toFloat(2);toInt(a/b),2500",
      "a:=toLong(5000);b:=toDouble(2);toInt(a/b),2500",

      "a:=toFloat(5000);b:=toFloat(2);toInt(a/b),2500",
      "a:=toFloat(5000);b:=toString(2);toInt(a/b),2500",
      "a:=toFloat(5000);b:=toInt(2);toInt(a/b),2500",
      "a:=toFloat(5000);b:=toLong(2);toInt(a/b),2500",
      "a:=toFloat(5000);b:=toDouble(2);toInt(a/b),2500",

      "a:=toDouble(5000);b:=toDouble(2);toInt(a/b),2500",
      "a:=toDouble(5000);b:=toString(2);toInt(a/b),2500",
      "a:=toDouble(5000);b:=toInt(2);toInt(a/b),2500",
      "a:=toDouble(5000);b:=toLong(2);toInt(a/b),2500",
      "a:=toDouble(5000);b:=toFloat(2);toInt(a/b),2500",

      "a:=toString(5000);b:=toDouble(2);toInt(a/b),2500",
      "a:=toString(5000);b:=toString(2);toInt(a/b),2500",
      "a:=toString(5000);b:=toInt(2);toInt(a/b),2500",
      "a:=toString(5000);b:=toLong(2);toInt(a/b),2500",
      "a:=toString(5000);b:=toFloat(2);toInt(a/b),2500"
    })
    void varVar(String equString, int expectedResult) {
        Equ<Integer> equ = Equ.builder(Integer.class)
                             .equation(equString)
                             .build();
        int result = equ.evaluate();
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @ValueSource(strings = {
      "toInt(a / 1)",
      "toInt(1/a)"
    })
    void unassigned(String equString) {
        Throwable exception = assertThrows(MathException.class, () -> {
            Equ.builder(Integer.class)
              .equation(equString)
              .build()
              .evaluate();
        });
        assertEquals("op(divide); \"a\" is unassigned", exception.getMessage());
    }
}
