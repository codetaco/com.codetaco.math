package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class VariableNamesTests {

    @Test
    void caseInsensitive() {

        double expected = 144;
        Double var = 12D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .variable("x", 12D)
                            .equation("x * X")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result);
        assertEquals(var, equ.getDouble("X"));
    }

    @Test
    void assignVariableFirst() {

        double expected = 246;
        Double var = 123D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .variable("x", 123D)
                            .equation("x * 2")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result);
        assertEquals(var, equ.getDouble("x"));
    }

    @Test
    void assignVariableSecond() {

        double expected = 246;
        Double var = 123D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .variable("x", 123D)
                            .equation("2 * x")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result);
        assertEquals(var, equ.getDouble("x"));
    }

    @Test
    void dots() {
        double expected = 17.2D;
        Double var = 17.2D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("myvar.inc:=17.2")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result);
        assertEquals(var, equ.getDouble("myvar.inc"));
    }

    @Test
    void doubleAsStringError() {
        try {
            Equ.builder(Double.class).equation("length(trim(2))").evaluate();
            fail("should have failed");
        } catch (Exception e) {
            assertEquals("function(trim); Literal required, found Long", e.getMessage());
        }
    }

    @Test
    void leadingDot() {
        double expected = 8.6D;
        Double var = 17.2D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("0.5*(_inc:=17.2)")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result);
        assertEquals(var, equ.getDouble("_inc"));
    }

    @Test
    void leadingUnderbar() {

        double expected = 17.2D;
        Double var = 17.2D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("_myvar.inc:=17.2")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result);
        assertEquals(var, equ.getDouble("_myvar.inc"));
    }

    @Test
    void undefinedVariableAsString() {

        double expected = 7D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("length(trim(unknown))")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result);
    }

    @Test
    void unquotedPhraseError() {
        try {
            Equ.builder(String.class).equation("length(trim(unquoted phrase))").evaluate();
            fail("should have failed");
        } catch (Exception e) {
            assertEquals("op(multiply); \"phrase\" is unassigned", e.getMessage());
        }
    }
}
