package com.codetaco.math;

import org.junit.jupiter.api.Test;

import java.time.Month;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class DatesTests {

    @Test
    void dateFormatter() {
        String result = Equ.builder(String.class)
                          .equation("toString(toDate('2018-10-03T19:27:09.390Z'))")
                          .build()
                          .evaluate();
        assertEquals("2018-10-03T00:00Z", result);
    }

    @Test
    void toDate() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("toDate('2018-10-03T19:27:09.390Z')")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-03T00:00Z", result.toString());
    }

    @Test
    void toDateWithFormat() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("toDate('20181003', 'yyyyMMdd')")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-03T00:00Z", result.toString());
    }

    @Test
    void date() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("date(toDate('2018-10-03T19:27:09.390Z'), '+3d'")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-06T00:00Z", result.toString());
    }

    @Test
    void dateTimeAdjNotAllowed() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("date(toDate('2018-10-03T19:27:09.390Z'), '=1min'")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-03T00:00Z", result.toString());
    }

    @Test
    void dateMissingAdj() {
        try {
            Equ.builder(Boolean.class)
              .equation("date(toDate('2018-10-03T19:27:09.390Z'))")
              .evaluate();
            fail("expected exception");
        } catch (Exception e) {
            assertEquals("missing operands for function(date)", e.getMessage());
        }
    }

    @Test
    void timeFormatter() {
        String result = Equ.builder(String.class)
                          .equation("toString(toTime('2018-10-03T19:27:09.390Z'))")
                          .build()
                          .evaluate();
        assertEquals("0000-01-01T19:27:09.390Z", result);
    }

    @Test
    void toTime() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("toTime('2018-10-03T19:27:09.390Z')")
                                 .build()
                                 .evaluate();
        assertEquals("0000-01-01T19:27:09.390Z", result.toString());
    }

    @Test
    void toTimeWithFormat() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("toTime('1927', 'HHmm')")
                                 .build()
                                 .evaluate();
        assertEquals("-999999999-01-01T19:27Z", result.toString());
    }

    @Test
    void time() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("time(toTime('2018-10-03T19:27:09.390Z'), '+3sec'")
                                 .build()
                                 .evaluate();
        assertEquals("0000-01-01T19:27:12.390Z", result.toString());
    }

    @Test
    void timeDateAdjNotAllowed() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("time(toTime('2018-10-03T19:27:09.390Z'), '=1year'")
                                 .build()
                                 .evaluate();
        assertEquals("0000-01-01T19:27:09.390Z", result.toString());
    }

    @Test
    void dateNotOnTime() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("date(toTime('2018-10-03T19:27:09.390Z'), '-1year'")
                                 .build()
                                 .evaluate();
        assertEquals("-0001-01-01T00:00Z", result.toString());
    }

    @Test
    void timeMissingAdj() {
        try {
            Equ.builder(Boolean.class)
              .equation("time(toTime('2018-10-03T19:27:09.390Z'))")
              .evaluate();
            fail("expected exception");
        } catch (Exception e) {
            assertEquals("missing operands for function(time)", e.getMessage());
        }
    }

    @Test
    void dateTimeFormatter() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("toDateTime('2018-10-03T19:27:09.390Z')")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-03T19:27:09.390Z", result.toString());
    }

    @Test
    void toDateTime() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("toDateTime('2018-10-03T19:27:09.390Z')")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-03T19:27:09.390Z", result.toString());
    }

    @Test
    void toDateTimeWithFormat() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("toDateTime('2018100311', 'yyyyMMddHH')")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-03T11:00Z", result.toString());
    }

    @Test
    void dateTimeFromDateTime() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("datetime(toDateTime('2018-10-03T19:27:09.390Z'), '+3d -1min'")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-06T19:26:09.390Z", result.toString());
    }

    @Test
    void dateTimeFromDate() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("datetime(toDateTime('2018-10-03T19:27:09.390Z'), '+3d'")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-06T19:27:09.390Z", result.toString());
    }

    @Test
    void dateTime() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("datetime(toDateTime('2018-10-03T19:27:09.390Z'), '+3d -1hour'")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-06T18:27:09.390Z", result.toString());
    }

    @Test
    void dateTimeMissingAdj() {
        try {
            Equ.builder(Boolean.class)
              .equation("dateTime(toDateTime('2018-10-03T19:27:09.390Z'))")
              .evaluate();
            fail("expected exception");
        } catch (Exception e) {
            assertEquals("missing operands for function(datetime)", e.getMessage());
        }
    }

    @Test
    void jsonDate() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("todate('2018-10-03T19:27:09.390Z')")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-03T00:00Z", result.toString());
    }

    @Test
    void jsonDateTime() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("todatetime('2018-10-03T19:27:09.390Z')")
                                 .build()
                                 .evaluate();
        assertEquals("2018-10-03T19:27:09.390Z", result.toString());
    }

    @Test
    void compareEqualsFalse() {
        assertFalse(Equ.builder(Boolean.class)
                      .equation("todate('19600409', 'yyyyMMdd') = todate('19600408', 'yyyyMMdd')")
                      .build()
                      .evaluate());
    }

    @Test
    void compareEqualsTrue() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("todate('19600409', 'yyyyMMdd') = todate('19600409', 'yyyyMMdd')")
                     .build()
                     .evaluate());
    }

    @Test
    void compareEqualsTrueWithoutAdjustments() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("todate('1960-04-09') = todate('19600409')")
                     .build()
                     .evaluate());
    }

    @Test
    void compareGreaterLess() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("todate('19600408', 'yyyyMMdd') < todate('19600409', 'yyyyMMdd')")
                     .build()
                     .evaluate());
    }

    @Test
    void compareGreaterTrue() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("todate('19600409', 'yyyyMMdd') > todate('19600408', 'yyyyMMdd')")
                     .build()
                     .evaluate());
    }

    @Test
    void createAndReturnADate() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("todate('19600409', 'yyyyMMdd')")
                                 .build()
                                 .evaluate();
        assertEquals(1960, result.getYear());
        assertEquals(Month.APRIL, result.getMonth());
        assertEquals(9, result.getDayOfMonth());
    }

    @Test
    void createAndReturnADate2() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("(todate('1960-04-09'))")
                                 .build()
                                 .evaluate();
        assertEquals("1960-04-09T00:00Z", result.toString());
    }

    @Test
    void modifyingCalendar2() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("dateTime(todatetime('196004090830', 'yyyyMMddHHmm'), '=10day =7h =31min'")
                                 .build()
                                 .evaluate();
        assertEquals(1960, result.getYear());
        assertEquals(Month.APRIL, result.getMonth());
        assertEquals(10, result.getDayOfMonth());
        assertEquals(7, result.getHour());
        assertEquals(31, result.getMinute());
    }

    @Test
    void modifyingCalendarWithoutTime() {
        ZonedDateTime result = Equ.builder(ZonedDateTime.class)
                                 .equation("date(todate('196004090830', 'yyyyMMddHHmm'), '=10day =7h =31min'")
                                 .build()
                                 .evaluate();
        assertEquals("1960-04-10T00:00Z", result.toString());
    }

    @Test
    void todayAndNow() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("todate('today') = date(todate('now'), '=0ho =0min =0sec =0milli')")
                     .build()
                     .evaluate());
    }

    @Test
    void withAdjustments() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("date(todate('1960-04-09'), '-1day') < todate('19600409')")
                     .build()
                     .evaluate());
    }

    @Test
    void withAdjustmentsFromStringVariable() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("DayNumber:=-1;date(todate('19600409', 'yyyyMMdd'), cat(toString(DayNumber, '%+1d'), 'day')) < todate('1960-04-09')")
                     .build()
                     .evaluate());
    }
}
