package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StringsTests {

    @Test
    void stringConcatinateMany() {

        String expected = "1234";
        String result = Equ.builder(String.class)
                          .equation("cat('1','2','3','4')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringConcatinateMatchResult() {

        String expected = "123WHAT";
        String result = Equ.builder(String.class)
                          .equation("cat(match('ABC123XYZ', '123'), 'WHAT')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringConcatinateMatchResults() {

        String expected = "123XYZ";
        String result = Equ.builder(String.class)
                          .equation("cat(match('ABC123XYZ', '123'), match('ABC123XYZ', 'XYZ'))")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringConcatinateOne() {

        String expected = "1";
        String result = Equ.builder(String.class)
                          .equation("cat('1')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringIndexOf() {
        double expected = 2D;
        double result = Equ.builder(Double.class)
                          .equation("indexOf(' ABC ', 'B')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringLeadingSpaces() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("' TRIMMED'=' TRIMMED'")
                     .evaluate());
    }

    @Test
    void stringLeftTrim() {

        String expected = "ABC ";
        String result = Equ.builder(String.class)
                          .equation("ltrim(' ABC ')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringLowercase() {

        String expected = "abcdef";
        String result = Equ.builder(String.class)
                          .equation("lcase('abcDEF')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringMetaphone() {

        String expected = "TLFN";
        String result = Equ.builder(String.class)
                          .equation("metaphone('telephone')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringMetaphoneCompare() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("metaphone('telephone') = metaphone('telifon')")
                     .evaluate());
    }

    @Test
    void stringNoSpaces() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("'TRIMMED'='TRIMMED'")
                     .evaluate());
    }

    @Test
    void stringReplaceWithEqual() {

        String expected = "ABC###XYZ";
        String result = Equ.builder(String.class)
                          .equation("replace('ABC123XYZ', '[0-9]', '#')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringReplaceWithLonger() {

        String expected = "ABC(NUMBER)(NUMBER)(NUMBER)XYZ";
        String result = Equ.builder(String.class)
                          .equation("replace('ABC123XYZ', '[0-9]', '(NUMBER)')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringReplaceWithShorter() {

        String expected = "ABC#3XYZ";
        String result = Equ.builder(String.class)
                          .equation("replace('ABC123XYZ', '[0-9]{2}', '#')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringRightTrim() {

        String expected = " ABC";
        String result = Equ.builder(String.class)
                          .equation("rtrim(' ABC ')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringSubstring() {

        String expected = "C12";
        String result = Equ.builder(String.class)
                          .equation("substr('ABC123XYZ', 2, 3)")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringTrailingSpaces() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("'TRIMMED '='TRIMMED '")
                     .evaluate());
    }

    @Test
    void stringTrim() {

        String expected = "ABC";
        String result = Equ.builder(String.class)
                          .equation("trim(' ABC ')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void stringUntrimmed() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("' TRIMMED '=' TRIMMED '")
                     .evaluate());
    }

    @Test
    void stringUppercase() {

        String expected = "ABCDEF";
        String result = Equ.builder(String.class)
                          .equation("ucase('abcDEF')")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void unquotedLiteral() {
        assertTrue(Equ.builder(Boolean.class)
                     .equation("Illinois:='Illinois';rtrim('Illinois') = Illinois")
                     .evaluate());
    }
}
