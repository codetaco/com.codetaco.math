package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PrecedenceTests {

    @Test
    void mdas01() {
        double expected = 0.5;
        double result = Equ.builder(Double.class)
                          .equation("2 * 3 / 4 + 5 - 6")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas02() {
        double expected = 0.5;
        double result = Equ.builder(Double.class)
                          .equation("5 - 6 + 3 / 4 * 2")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas03() {
        double expected = 2;
        double result = Equ.builder(Double.class)
                          .equation("2 * 3 / (4 + 5 - 6)")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas04() {
        double expected = 1.5D;
        double result = Equ.builder(Double.class)
                          .equation("2 * 3 / 4 ")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas05() {
        double expected = 1.5D;
        double result = Equ.builder(Double.class)
                          .equation("3 / 4 * 2")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas06() {
        double expected = 0.375D;
        double result = Equ.builder(Double.class)
                          .equation("3 / (4 * 2)")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas07() {
        double expected = 1D;
        double result = Equ.builder(Double.class)
                          .equation("3 / 4 + 1 / 4")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas08() {
        double expected = 0.15D;
        double result = Equ.builder(Double.class)
                          .equation("3 / (4 + 1) / 4")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas09() {
        long expected = 1024;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ 10")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas10() {
        long expected = 37;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ 5 + 5")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas11() {
        long expected = 1024;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ (5 + 5)")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas12() {
        long expected = 64;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ 5 * 2")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas13() {
        long expected = 1024;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ (5 * 2)")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas14() {
        long expected = 50;
        long result = Equ.builder(Long.class)
                        .equation("2 + 3 * 4 ^ 2")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas15() {
        long expected = 98;
        long result = Equ.builder(Long.class)
                        .equation("2 * (3 + 4) ^ 2")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas16() {
        long expected = 32;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ (2 + 3)")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas17() {
        long expected = 256;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ (2 + 3 * 2)")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas18() {
        long expected = 2048;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ (2 + 3 ^ 2)")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas18b() {
        long expected = 1024;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ (2 + 3) ^ 2")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas18c() {
        long expected = 13;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ 2 + 3 ^ 2")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas19() {
        long expected = 64;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ 3 ^ 2")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void mdas20() {
        long expected = 512;
        long result = Equ.builder(Long.class)
                        .equation("2 ^ (3 ^ 2)")
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void web01() {
        double expected = 3;
        double result = Equ.builder(Double.class)
                          .equation("a := 15; b := 3; t := 4; (a - b) / t")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void web02() {
        double expected = 4;
        double result = Equ.builder(Double.class)
                          .equation("a := 15; b := 3; t := 4; (2(a - b) + 3b - 5) / (t + b)")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void web03() {
        double expected = 28.8D;
        double result = Equ.builder(Double.class)
                          .equation("a := 15; b := 3; t := 4; 2(a - b)^2 / 10")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void web03ForcedDefaultAnyway() {
        double expected = 28.8D;
        double result = Equ.builder(Double.class)
                          .equation("a := 15; b := 3; t := 4; 2((a - b)^2) / 10")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void web03ForcedWrong() {
        double expected = 57.6D;
        double result = Equ.builder(Double.class)
                          .equation("a := 15; b := 3; t := 4; (2(a - b))^2 / 10")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void web04() {
        double expected = 30D;
        double result = Equ.builder(Double.class)
                          .equation("v := 5; t := 2; a := 10;v t + 1/2a t^2")
                          .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void web05() {
        double expected = 30D;
        double result = Equ.builder(Double.class)
                          .equation("d := 80; t := 2; a := 10;(d - 1/2 a t^2) / t")
                          .evaluate();
        assertEquals(expected, result);
    }
}
