package com.codetaco.math;

import com.codetaco.math.impl.EquImpl;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CompilingTests {
    @Test
    void duplicateVariablesAreReturnedOnce() {
        EquImpl impl = Equ.builder(Long.class)
                         .equation("rate(tablename, 'a', alpha(tablename))")
                         .build()
                         .getEquImpl();
        final Set<String> variableNames = impl.gatherVariables();
        assertEquals(1, variableNames.size());
        final String[] v = variableNames.toArray(new String[variableNames.size()]);
        assertEquals("tablename", v[0]);
    }

    @Test
    void literalsAreNotInVariableReturns() {
        EquImpl impl = Equ.builder(Long.class)
                         .equation("rate(tablename, 'a', alpha(FacAmt))")
                         .build()
                         .getEquImpl();

        final Set<String> variableNames = impl.gatherVariables();
        assertEquals(2, variableNames.size());
        final String[] v = variableNames.toArray(new String[variableNames.size()]);
        assertEquals("FacAmt", v[0]);
        assertEquals("tablename", v[1]);
    }
}
