package com.codetaco.math;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class ProfilingMain {

    public static void main(String[] args) {
        ProfilingMain main = new ProfilingMain();
        main.patterns();
    }

    private void patterns() {
        IntStream.range(1, 100001).forEach(
          (i) ->
          {
              try {
                  Pattern pattern = patterns1(i);
                  Matcher matcher = patterns2(pattern, "abc123xyz");
                  if (!patterns3(matcher)) {
                      System.out.println("WHAT " + i);
                  }

              } catch (Exception e) {
                  System.out.println(e.getMessage());
              }
          });
    }

    private Pattern patterns1(int midPart) {
        String pattern = "[^0-9]*([123" + midPart + "]+)[^0-9]*";
//        System.out.println(pattern);
        return Pattern.compile(pattern);
    }

    private Matcher patterns2(Pattern pattern, String target) {
        return pattern.matcher(target);
    }

    private boolean patterns3(Matcher matcher) {
        return matcher.find();
    }
}
