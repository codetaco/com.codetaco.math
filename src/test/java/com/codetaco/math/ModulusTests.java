package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ModulusTests {
    @Test
    void mod1() {
        long result = Equ.builder(Long.class).equation("13%10").build().evaluate();
        assertEquals(3, result);
    }

    @Test
    void mod2() {
        double result = Equ.builder(Double.class).equation("round(13.76%3, 2)").build().evaluate();
        assertEquals(1.76, result);
    }

    @Test
    void mod3() {
        boolean result = Equ.builder(Boolean.class).equation("85 % 2 = 1").build().evaluate();
        assertTrue(result);
    }
}
