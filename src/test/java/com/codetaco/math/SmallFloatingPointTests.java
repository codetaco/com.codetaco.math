package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SmallFloatingPointTests {

    private double delta = 0.000000001;

    @Test
    void floatMin() {

        double expected = 1.4E-45;
        double result = Equ.builder(Double.class)
                          .equation("" + Float.MIN_VALUE)
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void doubleMin() {

        double expected = 4.9E-324;
        double result = Equ.builder(Double.class)
                          .equation("" + Double.MIN_VALUE)
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void smallerThanMin() {

        double expected = 0.0;
        double result = Equ.builder(Double.class)
                          .equation("" + Double.MIN_VALUE + "/ 2")
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void eMinus1() {

        double expected = 12.464758;
        double result = Equ.builder(Double.class)
                          .equation("124.64758E-1")
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void eMinus2() {

        double expected = -5.43656365691809;
        double result = Equ.builder(Double.class)
                          .equation("E-2")
                          .evaluate();
        assertEquals(expected, result, delta);
    }
}
