package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LargeFloatingPointTests {

    private double delta = 0.000000001;

    @Test
    void floatMax() {
        double expected = 3.4028235E38D;
        double result = Equ.builder(Double.class)
                          .equation("" + Float.MAX_VALUE)
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void doubleMax() {
        double expected = 1.7976931348623157E308D;
        double result = Equ.builder(Double.class)
                          .equation("" + Double.MAX_VALUE)
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void biggerThanMax() {
        double expected = Double.POSITIVE_INFINITY;
        double result = Equ.builder(Double.class)
                          .equation("2*" + Double.MAX_VALUE)
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void e7() {
        double expected = 1.2464758E7;
        double result = Equ.builder(Double.class)
                          .equation("1.2464758E7")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void e() {
        double expected = 2.718281828459045D;
        double result = Equ.builder(Double.class)
                          .equation("E")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void timesE() {
        double expected = 5.43656365691809D;
        double result = Equ.builder(Double.class)
                          .equation("2 E")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void timesEspaceless() {
        double expected = 5.43656365691809D;
        double result = Equ.builder(Double.class)
                          .equation("2E")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void timesEtimes() {
        double expected = 10.87312731383618D;
        double result = Equ.builder(Double.class)
                          .equation("2 E 2")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void timesEtimesSpaceless() {
        double expected = 200L;
        double result = Equ.builder(Double.class)
                          .equation("2E2")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void eeVariable() {
        long expected = 4L;
        long result = Equ.builder(Long.class)
                        .equation("EE2 := 2;2EE2")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void eTimes() {
        double expected = 5.43656365691809D;
        double result = Equ.builder(Double.class)
                          .equation("E 2")
                          .build()
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void variable() {
        long expected = 7L;
        long result = Equ.builder(Integer.class)
                        .equation("E2")
                        .variable("E2", 7)
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }
}
