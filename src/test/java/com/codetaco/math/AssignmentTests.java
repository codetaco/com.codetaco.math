package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

class AssignmentTests {

    @Test
    void divideEquals() {

        Long expectedResult = 25L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("green := 50; green /= 2")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedResult, equ.getLong("green"));
    }

    @Test
    void multiplyEquals() {

        Long expectedResult = 10L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("green := 5; green *= 2")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedResult, equ.getLong("green"));
    }

    @Test
    void minusEquals() {

        Long expectedResult = 3L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("green := 5; green -= 2")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedResult, equ.getLong("green"));

    }

    @Test
    void plusEquals() {

        Long expectedResult = 4L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("green := 2; green += 2")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedResult, equ.getLong("green"));
    }

    @Test
    void plusEqualsTwice() {

        Long expectedResult = 7L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("green := 2; (green += 2) += 3")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedResult, equ.getLong("green"));
    }

    @Test
    void plusEqualsNothingInGreen() {
        try {
            Equ.builder(Long.class)
              .equation("green += 2")
              .build()
              .evaluate();
            fail("expected exception");
        } catch (Exception e) {
            assertEquals("invalid assignment target: var(green)",
                         e.getMessage());
        }
    }

    @Test
    void selfAssignment() {

        Long expectedResult = 4L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("green := 2; green := green + 2")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedResult, equ.getLong("green"));

    }

    @Test
    void conditionalAssignmentTarget() {

        Long expectedResult = 1L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("if (1=2, a, b) := 1")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertNull(equ.getLong("a"));
        assertEquals(expectedResult, equ.getLong("b"));
    }

    @Test
    void equalsInsteadOfAssignment() {
        try {
            Equ.builder(Long.class)
              .equation("max (a := 1, b=2)")
              .build()
              .evaluate();
            fail("expected exception");
        } catch (Exception e) {
            assertEquals(
              "op(compare equal); \"b\" is unassigned",
              e.getMessage());
        }
    }

    @Test
    void finalized() {

        Long expectedResult = 2L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("max (a := 1, a :=2)")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedResult, equ.getLong("a"));
    }

    @Test
    void finalized2() {

        Long expectedResult = 3L;
        Long expectedVar = 2L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("max (a := 3, a :=2)")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedVar, equ.getLong("a"));
    }

    @Test
    void internalAssignments() {

        Long expectedResult = 2L;
        Long expectedV1 = 1L;
        Long expectedV2 = 2L;
        Long expectedV3 = 2L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("c:=max (a := 1, b :=2)")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedV1, equ.getLong("a"));
        assertEquals(expectedV2, equ.getLong("b"));
        assertEquals(expectedV3, equ.getLong("c"));
    }

    @Test
    void invalidTarget() {
        try {
            Equ.builder(Long.class)
              .equation("'target' :=2")
              .build()
              .evaluate();
            fail("expected exception");
        } catch (Exception e) {
            assertEquals("invalid assignment target: target", e.getMessage());
        }
    }

    @Test
    void multipleAssignments() {

        Long expectedResult = 3L;
        Long expectedV1 = 3L;
        Long expectedV2 = 3L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("a := (b := 3)")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedV1, equ.getLong("a"));
        assertEquals(expectedV2, equ.getLong("b"));
    }

    @Test
    void multipleAssignments2() {

        Long expectedResult = 3L;
        Long expectedV1 = 3L;
        Long expectedV2 = 3L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("a := b := 3")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedV1, equ.getLong("a"));
        assertEquals(expectedV2, equ.getLong("b"));
    }

    @Test
    void multipleAssignments3() {
        try {
            Equ.builder(Long.class)
              .equation("a := 3 := b")
              .build()
              .evaluate();
            fail("expected exception");
        } catch (Exception e) {
            assertEquals("invalid assignment value: var(b)", e.getMessage());
        }
    }

    @Test
    void unknownVariableInComparison() {

        try {
            Equ.builder(Long.class)
              .equation("b=2")
              .build()
              .evaluate();
        } catch (Exception e) {
            assertEquals(
              "op(compare equal); \"b\" is unassigned",
              e.getMessage());
        }
    }

    @Test
    void withoutSupport() {

        Long expectedResult = 1L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("a := 1")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedResult, equ.getLong("a"));
    }

    @Test
    void withSupport() {

        Long expectedResult = 1L;
        Equ<Long> equ = Equ.builder(Long.class)
                          .equation("a := 1")
                          .build();
        assertEquals(expectedResult, equ.evaluate());
        assertEquals(expectedResult, equ.getLong("a"));
    }
}
