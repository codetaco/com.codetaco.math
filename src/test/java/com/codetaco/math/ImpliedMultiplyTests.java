package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ImpliedMultiplyTests {
    @Test
    void factorialMinus1() {

        long expected = 4L;
        long result = Equ.builder(Long.class)
                        .equation("3!- 2")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void factorialMinus2() {

        long expected = -12L;
        long result = Equ.builder(Long.class)
                        .equation("3!-2")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void factorialMinus3() {

        long expected = 4L;
        long result = Equ.builder(Long.class)
                        .equation("a:=2;3!-a")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void factorialMinus4() {

        long expected = -12L;
        long result = Equ.builder(Long.class)
                        .equation("a:=2;-a 3!")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void factorialMinus5() {

        long expected = -12L;
        long result = Equ.builder(Long.class)
                        .equation("a:=2;(-a)3!")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void functionMinus1() {

        long expected = -24L;
        long result = Equ.builder(Long.class)
                        .equation("min(12,13) -2")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void functionMinus2() {

        long expected = 10L;
        long result = Equ.builder(Long.class)
                        .equation("min(12,13) - 2")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void negativeVariable1() {

        long expected = -4L;
        long result = Equ.builder(Long.class)
                        .equation("a:=4;-a")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void negativeVariable2() {

        long expected = -4L;
        long result = Equ.builder(Long.class)
                        .equation("a:=4;- a")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void numberVariable1() {

        long expected = 8L;
        long result = Equ.builder(Long.class)
                        .equation("a:=4;2a")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void numberVariable2() {

        long expected = 8L;
        long result = Equ.builder(Long.class)
                        .equation("a:=4;2 a")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void numberVariable3() {

        long expected = -8L;
        long result = Equ.builder(Long.class)
                        .equation("a:=4;-2a")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void numberVariable4() {

        long expected = -2L;
        long result = Equ.builder(Long.class)
                        .equation("a:=4;2 -a")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void numberVariable5() {

        long expected = -2L;
        long result = Equ.builder(Long.class)
                        .equation("a:=4;2-a")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void timesNegative1() {

        long expected = -20L;
        long result = Equ.builder(Long.class)
                        .equation("a:=4;5*-a")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }

    @Test
    void timesNegative2() {

        long expected = -20L;
        long result = Equ.builder(Long.class)
                        .equation("a:=4;5*- a")
                        .build()
                        .evaluate();
        assertEquals(expected, result);
    }
}
