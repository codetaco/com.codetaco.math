package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ReuseTests {

    @Test
    void reuseEqu() {

        double expected = 10;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("2x")
                            .variable("x", 5D)
                            .build();
        double result = 0;
        for (int x = 0; x <= 1000; x++) {
            result = equ.evaluate();
        }
        assertEquals(expected, result);

        for (double x = 0; x <= 400000; x++) {
            equ.variable("x", x);
            assertEquals((Double) (2 * x), equ.evaluate());
        }
    }
}
