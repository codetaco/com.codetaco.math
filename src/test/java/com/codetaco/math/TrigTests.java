package com.codetaco.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TrigTests {

    private static final double delta = 0.01D;

    @Test
    void usingSinTwice() {

        double expected = 0.842D;
        double result = Equ.builder(Double.class)
                          .equation("sin(20) + sin(30)")
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void web01() {

        double expected = 5;
        double result = Equ.builder(Double.class)
                          .equation("10 sin(30)")
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void web01a() {

        double expected = 5;
        double result = Equ.builder(Double.class)
                          .equation("10sin(deg(0.5236))")
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void web01b() {

        double expected = 0.866D;
        double result = Equ.builder(Double.class)
                          .equation("cos(30)")
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void web02() {

        double expected = 2.8D;
        double result = Equ.builder(Double.class)
                          .equation("4 / tan(55)")
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void web02a() {

        double expected = 4.88D;
        double result = Equ.builder(Double.class)
                          .equation("a := 4 / tan(55); sqrt(4^2 + a^2)")
                          .evaluate();
        assertEquals(expected, result, delta);
    }

    @Test
    void web03() {

        double expected = 48.6;
        double varA = 41.4;
        double varB = 48.6;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("a:=acos(3/4); b:=90-a")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result, delta);
        assertEquals(varA, equ.getDouble("a"), delta);
        assertEquals(varB, equ.getDouble("b"), delta);
    }

    @Test
    void web04() {

        double expected = 30.96D;
        double varA = 59.04D;
        double varB = 30.96D;
        double varC = 5.83D;
        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("c:=sqrt(3^2+5^2);a:=atan(5/3);b:=90-a")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result, delta);
        assertEquals(varA, equ.getDouble("a"), delta);
        assertEquals(varB, equ.getDouble("b"), delta);
        assertEquals(varC, equ.getDouble("c"), delta);
    }

    @Test
    void web05() {
        double expected = 22.66D;
        double varV = 10.57D;
        double varH = 22.66D;

        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("v:=25sin(25);h:=25cos(25)")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result, delta);
        assertEquals(varV, equ.getDouble("v"), delta);
        assertEquals(varH, equ.getDouble("h"), delta);
    }

    @Test
    void web06() {
        double expected = 9.99D;
        double varA = 0.339D;
        double varV = 9.99D;

        Equ<Double> equ = Equ.builder(Double.class)
                            .equation("a:=3.21/9.47;atana:=atan(a);v:=sqrt(3.21^2+9.47^2)")
                            .build();
        double result = equ.evaluate();
        assertEquals(expected, result, delta);
        assertEquals(varA, equ.getDouble("a"), delta);
        assertEquals(varV, equ.getDouble("v"), delta);
    }
}
