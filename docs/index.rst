Math by CodeTaco
================

.. image:: /images/codetaco.png
    :width: 120px
    :alt: CodeTaco logo
    :align: right

.. image:: /images/appIcon.png
    :width: 120px
    :alt: Math logo
    :align: left

"Math" is a library ...

Things to do
------------

`Read the Javadocs <http://www.javadoc.io/doc/com.codetaco/math>`_

`Get the code to include this in your project's dependencies <https://search.maven.org/search?q=g:com.codetaco%20AND%20a:math&core=gav>`_


:ref:`Keyword Index <genindex>`, :ref:`Search Page <search>`

See date for a table of contents example
